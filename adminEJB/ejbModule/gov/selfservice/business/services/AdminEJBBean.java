package gov.selfservice.business.services;

import gov.selfservice.business.customEntities.CUSTOM_ADMIN_SEARCH_Cargo;
import gov.selfservice.business.customEntities.CUSTOM_ADMIN_SEARCH_Collection;
import gov.selfservice.business.customEntities.CUSTOM_ADMIN_WORKER_INBOX_Cargo;
import gov.selfservice.business.customEntities.CUSTOM_ADMIN_WORKER_INBOX_Collection;
import gov.selfservice.business.customEntities.CUSTOM_REFERENCE_TABLE_DATA_Cargo;
import gov.selfservice.business.customEntities.CUSTOM_REFERENCE_TABLE_DATA_Collection;
import gov.selfservice.business.customEntities.CUSTOM_SURVEY_SEARCH_Cargo;
import gov.selfservice.business.customEntities.CUSTOM_SURVEY_SEARCH_Collection;
import gov.selfservice.business.entities.ADM_BF_ASSIG_Cargo;
import gov.selfservice.business.entities.ADM_BF_ASSIG_Collection;
import gov.selfservice.business.entities.ADM_BUS_FUNC_Cargo;
import gov.selfservice.business.entities.ADM_BUS_FUNC_Collection;
import gov.selfservice.business.entities.ADM_RES_ASSIG_Cargo;
import gov.selfservice.business.entities.ADM_RES_ASSIG_Collection;
import gov.selfservice.business.entities.ADM_ROLE_Cargo;
import gov.selfservice.business.entities.ADM_ROLE_Collection;
import gov.selfservice.business.entities.ADM_USER_Cargo;
import gov.selfservice.business.entities.ADM_USER_Collection;
import gov.selfservice.business.entities.ADM_USR_ASSIG_Cargo;
import gov.selfservice.business.entities.ADM_USR_ASSIG_Collection;
import gov.selfservice.business.entities.CP_NOTIFICATIONS_Cargo;
import gov.selfservice.business.entities.CP_NOTIFICATIONS_Collection;
import gov.selfservice.business.entities.DPLY_TXT_Cargo;
import gov.selfservice.business.entities.DPLY_TXT_Collection;
import gov.selfservice.business.entities.LKUP_Cargo;
import gov.selfservice.business.entities.LKUP_Collection;
import gov.selfservice.business.entities.LKUP_FLD_Cargo;
import gov.selfservice.business.entities.LKUP_FLD_Collection;
import gov.selfservice.business.entities.LKUP_GRP_Cargo;
import gov.selfservice.business.entities.LKUP_GRP_Collection;
import gov.selfservice.business.entities.LKUP_GRP_FLD_Cargo;
import gov.selfservice.business.entities.LKUP_GRP_FLD_Collection;
import gov.selfservice.business.entities.MSG_Cargo;
import gov.selfservice.business.entities.MSG_Collection;
import gov.selfservice.business.entities.PAGE_Cargo;
import gov.selfservice.business.entities.PAGE_Collection;
import gov.selfservice.business.entities.RMC_RQST_HIST_Cargo;
import gov.selfservice.business.entities.RMC_RQST_HIST_Collection;
import gov.selfservice.business.rules.SecurityHelperBO;
import gov.selfservice.business.rules.admin.AdminSearchBO;
import gov.selfservice.business.rules.admin.AdminSurveyViewerBO;
import gov.selfservice.business.validation.admin.ADCIBValidator;
import gov.selfservice.business.validation.admin.ADIBXValidator;
import gov.selfservice.business.validation.admin.ADSVRValidator;
import gov.selfservice.business.validation.admin.search.ADCBFValidator;
import gov.selfservice.business.validation.admin.search.ADCEDValidator;
import gov.selfservice.business.validation.admin.search.ADCRRValidator;
import gov.selfservice.business.validation.admin.search.ADDSPValidator;
import gov.selfservice.business.validation.admin.search.ADEDTValidator;
import gov.selfservice.business.validation.admin.search.ADEMDValidator;
import gov.selfservice.business.validation.admin.search.ADMASValidator;
import gov.selfservice.business.validation.admin.search.ADMBFValidator;
import gov.selfservice.business.validation.admin.search.ADMSGValidator;
import gov.selfservice.business.validation.admin.search.ADMTRValidator;
import gov.selfservice.business.validation.admin.search.ADRSRValidator;
import gov.selfservice.business.validation.admin.search.ADSCRValidator;
import gov.selfservice.business.validation.admin.search.ADSFUValidator;
import gov.selfservice.framework.business.entities.FwTransaction;
import gov.selfservice.framework.exceptions.FwException;
import gov.selfservice.framework.exceptions.FwWrappedException;
import gov.selfservice.framework.management.constants.FwConstants;
import gov.selfservice.framework.management.logging.ILog;
import gov.selfservice.framework.management.util.FwDate;
import gov.selfservice.framework.presentation.entities.listview.IListviewFormatter;
import gov.selfservice.management.constants.AppConstants;
import gov.selfservice.management.constants.CafeConstants;
import gov.selfservice.presentation.entities.listview.ADAnnouncementsSearchDplyListView;
import gov.selfservice.presentation.entities.listview.ADCCWorkerInboxResultsListView;
import gov.selfservice.presentation.entities.listview.ADMaxstarSearchResultsListview;
import gov.selfservice.presentation.entities.listview.ADReferenceDataListView;
import gov.selfservice.presentation.entities.listview.ADSearchDomainListView;
import gov.selfservice.presentation.entities.listview.ADSearchDplyListView;
import gov.selfservice.presentation.entities.listview.ADSearchMsgListView;
import gov.selfservice.presentation.entities.listview.ADSearchResultsListview;
import gov.selfservice.presentation.entities.listview.ADSearchTablesListView;
import gov.selfservice.presentation.entities.listview.ADSearchUserListView;
import gov.selfservice.presentation.entities.listview.ADWorkerInboxResultsListView;
import gov.selfservice.management.constants.CafeConstants;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJBException;
import javax.ejb.Stateless;

/**
 * AdminEJB
 * 
 * @author Rambabu Kavuri
 * 
 */
@Stateless

public class AdminEJBBean extends AppSessionBean  implements AdminEJBLocal {

	/**
	 * Constructor
	 */
	public AdminEJBBean() {
	}
	public static final String AfterUpdate = "AfterUpdate";
	public static final String OnInbox = "onInbox";
	/**
	 * This method is used to retrieve all the applications that match the given
	 * criteria.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void getEFormSearchResults(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::getEFormSearchResults::Start");

		try {

			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			String langInd = FwConstants.ENGLISH; // Language Indicator
			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				langInd = (String) httpSession.get(AppConstants.LANGUAGE);
			}
			CUSTOM_ADMIN_SEARCH_Collection appPgmRqstAfterColl = (CUSTOM_ADMIN_SEARCH_Collection) pageCollection
					.get(AppConstants.CUSTOM_ADMIN_SEARCH_Collection); // Custom Admin
			// Search Collection
			CUSTOM_ADMIN_SEARCH_Cargo customAdminSearchCargo = appPgmRqstAfterColl
					.getResult(0); // Custom Admin Search Cargo
			ADSCRValidator adscrValidator = new ADSCRValidator();
			adscrValidator.validateEFormSearchDetails(customAdminSearchCargo);
			if (adscrValidator.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, adscrValidator
						.getMessageList());
				pageCollection.put("CUSTOM_ADMIN_SEARCH",
						customAdminSearchCargo);
				return;
			}

			AdminSearchBO adminSearchBO = new AdminSearchBO();
			CUSTOM_ADMIN_SEARCH_Collection searchResults = adminSearchBO
					.loadSearchResults(customAdminSearchCargo);
			if (searchResults == null) {
				adscrValidator
						.validateEFormSearchDetails(customAdminSearchCargo);
				if (adscrValidator.hasMessages()) {
					request.put(FwConstants.MESSAGE_LIST, adscrValidator
							.getMessageList());
					return;
				}
			}
			
			if (searchResults != null && searchResults.size() > 100) {
				pageCollection.put("MORE_RECORDS", CafeConstants.TRUE);
			}
			String tabId = (String)request.get(FwConstants.TAB_ID);
			ADSearchResultsListview listviewbean = new ADSearchResultsListview();
			listviewbean.setLanguage(langInd);
			listviewbean.setTabId(tabId);
			// call the set DisplayData
			listviewbean.setDisplayData(searchResults);
			// uniquely name the listview
			listviewbean.setName(AppConstants.ADSearchResultsListview_Listview);
			// this oen for the paging
			listviewbean.setPageSize(20);
			// define the columns to be sorted on takes a boolean array
			boolean sortableCols[] = { false, false, false, false, false,false };
			listviewbean.setSortableColumns(sortableCols);
			// specify the type of the columns in listview
			int[] dataType = { IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol, 
					IListviewFormatter.stringCol };
			listviewbean.setColDataType(dataType);
			List<String> appNumList = listviewbean.getAppNumList();
			session.put(AppConstants.APP_NUM_LIST, appNumList);
			pageCollection
					.put(AppConstants.ADSearchResultsListview_Listview, listviewbean);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"getEFormSearchResults",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("getEFormSearchResults");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"getEFormSearchResults", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::getEFormSearchResults::End");
	}

	/**
	 * This method is used to retrieve reference table data for a given
	 * reference table name.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void getRefTableData(FwTransaction txnBean)
			throws javax.ejb.EJBException {

		log(ILog.INFO, "AdminEJBBean::getRefTableData::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			String langInd = FwConstants.ENGLISH; // Language Indicator
			String tableId = (String) request.get("appNumber"); // Table Id
			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				langInd = (String) httpSession.get(AppConstants.LANGUAGE);
			}

			AdminSearchBO adminSearchBO = new AdminSearchBO();

			LKUP_GRP_Collection searchResults = adminSearchBO
					.loadReferenceTables(tableId); // search result


			// More than 100 records warning message.
			if (adminSearchBO.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, adminSearchBO.getMessageList());
			}
			
			
			ADSearchTablesListView listviewbean = new ADSearchTablesListView();
			listviewbean.setLanguage(langInd);
			// call the set DisplayData
			listviewbean.setDisplayData(searchResults);
			// uniquely name the listview
			listviewbean.setName(AppConstants.ADSearchTablesListView_Listview);
			// this oen for the paging
			listviewbean.setPageSize(20);
			// define the columns to be sorted on takes a boolean array
			boolean sortableCols[] = { false, false, false, false };
			listviewbean.setSortableColumns(sortableCols);
			// specify the type of the columns in listview
			int[] dataType = { IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol,
					IListviewFormatter.stringCol };
			listviewbean.setColDataType(dataType);
			pageCollection.put(AppConstants.ADSearchTablesListView_Listview, listviewbean);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"getRefTableData",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("getRefTableData");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"getRefTableData", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::getRefTableData::End");
	}

	/**
	 * This method is used to retrieve reference table column names for a given
	 * reference table name.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void getRefTableDomains(FwTransaction txnBean)
			throws javax.ejb.EJBException {

		log(ILog.INFO, "AdminEJBBean::getRefTableDomains::Start");
		try {
			Map session = txnBean.getSession(); // Session Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			String langInd = FwConstants.ENGLISH; // Language Indicator
			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				langInd = (String) httpSession.get(AppConstants.LANGUAGE);
			}

			AdminSearchBO adminSearchBO = new AdminSearchBO();

			LKUP_FLD_Collection searchResults = adminSearchBO
					.loadReferenceTablesDomains(); // Lkup Fld Collection
			ADSearchDomainListView listviewbean = new ADSearchDomainListView();
			listviewbean.setLanguage(langInd);
			// call the set DisplayData
			listviewbean.setDisplayData(searchResults);
			// uniquely name the listview
			listviewbean.setName(AppConstants.ADSearchDomainListView_Listview);
			// this oen for the paging
			listviewbean.setPageSize(20);
			// define the columns to be sorted on takes a boolean array
			boolean sortableCols[] = { false, false, false, false };
			listviewbean.setSortableColumns(sortableCols);
			// specify the type of the columns in listview
			int[] dataType = { IListviewFormatter.stringCol,
					IListviewFormatter.stringCol };
			listviewbean.setColDataType(dataType);
			pageCollection.put(AppConstants.ADSearchDomainListView_Listview, listviewbean);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"getRefTableDomains",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("getRefTableDomains");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"getRefTableDomains", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::getRefTableDomains::End");
	}

	/**
	 * This method loads the reference table column details for editing/adding.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadRefTableDomain(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::loadRefTableDomain::Start");

		try {

			Map session = txnBean.getSession(); // Session Object
			Map pageCollection = txnBean.getPageCollection(); // Page
		
			LKUP_FLD_Collection appPgmRqstAfterColl = (LKUP_FLD_Collection) pageCollection
					.get(AppConstants.LKUP_FLD_Collection); // Lkup Fld Collection
			LKUP_FLD_Cargo customAdminSearchCargo = appPgmRqstAfterColl
					.getCargo(0); // Lkup Fld Cargo

			pageCollection.put("LKUP_FLD_SEARCH", customAdminSearchCargo);
			session.put("LKUP_FLD_SEARCH", customAdminSearchCargo);
			ADSearchDomainListView listviewbean = new ADSearchDomainListView();

			if (session.get(FwConstants.BEFORE_COLLECTION) != null) {
				Map pCollection = (Map) session
						.get(FwConstants.BEFORE_COLLECTION);

				if (pCollection.get(AppConstants.ADSearchDomainListView_Listview) != null) {

					listviewbean = (ADSearchDomainListView) pCollection
							.get(AppConstants.ADSearchDomainListView_Listview);

				}
			}

			pageCollection.put(AppConstants.ADSearchDomainListView_Listview, listviewbean);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"loadRefTableDomain",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("loadRefTableDomain");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"loadRefTableDomain", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::loadRefTableDomain::End");
	}

	/**
	 * This method stores the reference table columns names.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void sotreRefTableDomains(FwTransaction txnBean)
			throws javax.ejb.EJBException {

		log(ILog.INFO, "AdminEJBBean::sotreRefTableDomains::Start");

		try {

			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			
			AdminSearchBO adminSearchBO = new AdminSearchBO();
			String fldId = null; // Column Id
			String fldDesc = null; // Column Description

			LKUP_FLD_Cargo cargo = new LKUP_FLD_Cargo(); // Lkup Fld Cargo
			LKUP_FLD_Collection collection = new LKUP_FLD_Collection(); // Lkup
		
			fldDesc = (String) request.get("appNumber");
			fldId = (String) request.get("fld_id");

			ADRSRValidator adrsrValidator = new ADRSRValidator();
			adrsrValidator.validateRTDomain(fldDesc);
			if (adrsrValidator.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, adrsrValidator
						.getMessageList());
				ADSearchDomainListView listviewbean = new ADSearchDomainListView();

				if (session.get(FwConstants.BEFORE_COLLECTION) != null) {
					Map pCollection = (Map) session
							.get(FwConstants.BEFORE_COLLECTION);

					if (pCollection.get(AppConstants.ADSearchDomainListView_Listview) != null) {

						listviewbean = (ADSearchDomainListView) pCollection
								.get(AppConstants.ADSearchDomainListView_Listview);

					}
				}

				pageCollection.put(AppConstants.ADSearchDomainListView_Listview,
						listviewbean);
				session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

				return; // return to jsp
			} else {
				String maxNumber = adminSearchBO.checkLKUPDomain(fldDesc);
				if (maxNumber != null && !maxNumber.equalsIgnoreCase("0")) {
					adrsrValidator.validateRTData();
					if (adrsrValidator.hasMessages()) {
						request.put(FwConstants.MESSAGE_LIST, adrsrValidator
								.getMessageList());
						ADSearchDomainListView listviewbean = new ADSearchDomainListView();

						if (session.get(FwConstants.BEFORE_COLLECTION) != null) {
							Map pCollection = (Map) session
									.get(FwConstants.BEFORE_COLLECTION);

							if (pCollection
									.get(AppConstants.ADSearchDomainListView_Listview) != null) {

								listviewbean = (ADSearchDomainListView) pCollection
										.get(AppConstants.ADSearchDomainListView_Listview);

							}
						}

						pageCollection.put(AppConstants.ADSearchDomainListView_Listview,
								listviewbean);
						session.put(FwConstants.BEFORE_COLLECTION,
								pageCollection);

						return; // return to jsp
					}
				}

			}
			if (fldId != null && !fldId.equals("") && !fldId.equals("null") ) {
				cargo.setLkup_fld_id(fldId);
				cargo.setLkup_fld_dsc(fldDesc);
				cargo.setRowAction(FwConstants.ROWACTION_UPDATE);
			} else {

				String maxNum = adminSearchBO.getLKUPFLDMaxNum();
				cargo.setLkup_fld_id("" + ((Integer.parseInt(maxNum)) + 1));
				cargo.setLkup_fld_dsc(fldDesc);
				cargo.setRowAction(FwConstants.ROWACTION_INSERT);

			}
			collection.add(cargo);
			adminSearchBO.storeReferenceTablesDomains(collection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"sotreRefTableDomains",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("sotreRefTableDomains");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::sotreRefTableDomains::End");
	}

	/**
	 * This method loads the reference table data for a given reference table
	 * name.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadReferenceData(FwTransaction txnBean)
			throws javax.ejb.EJBException {

		log(ILog.INFO, "AdminEJBBean::loadReferenceData::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			String langInd = FwConstants.ENGLISH; // Language Indicator
			String tableId = null; // Table Id
			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				langInd = (String) httpSession.get(AppConstants.LANGUAGE);
			}

			// Removing Previous session Values

			session.remove(CafeConstants.REFERENCE_TABLE_DATA_COLLECTION);

			CUSTOM_REFERENCE_TABLE_DATA_Collection customRefDataColl = (CUSTOM_REFERENCE_TABLE_DATA_Collection) pageCollection
					.get(AppConstants.CUSTOM_REFERENCE_TABLE_DATA_Collection);
			if (customRefDataColl != null) {
				CUSTOM_REFERENCE_TABLE_DATA_Cargo customRefDataCargo = customRefDataColl
						.getResult(0);

				tableId = customRefDataCargo.getLkup_grp_cd();
			} else {
				tableId = (String) request.get("referenceValues_tablename");
			}

			String refTableId = null; // Reference Table Id
			String refTabDesc = null; // Reference Table Description

			AdminSearchBO adminSearchBO = new AdminSearchBO();

			CUSTOM_REFERENCE_TABLE_DATA_Collection refDataCollection = adminSearchBO
					.loadReferenceData(tableId); // Cutom Reference Table
			// Data Collection
			CUSTOM_REFERENCE_TABLE_DATA_Collection filterCollection = new CUSTOM_REFERENCE_TABLE_DATA_Collection(); // Cutom
			// Reference
			// Table
			// Data
			// Collection
			CUSTOM_REFERENCE_TABLE_DATA_Cargo filterCargo = null; // Cutom
			// Reference
			// Table
			// Data
			// Cargo
			CUSTOM_REFERENCE_TABLE_DATA_Cargo tmpCargo = null; // Cutom
			// Reference
			// Table Data
			// Cargo
			if (refDataCollection != null && refDataCollection.size() > 0) {

				tmpCargo = (CUSTOM_REFERENCE_TABLE_DATA_Cargo) refDataCollection
						.get(0);
				String lkupFldId = tmpCargo.getLkup_fld_id(); // Lookup field
				// id
				refTableId = tmpCargo.getLkup_grp_cd();
				refTabDesc = tmpCargo.getLkup_grp_dsc();

				int size = refDataCollection.size();

				for (int i = 0; i < size; i++) {
					filterCargo = (CUSTOM_REFERENCE_TABLE_DATA_Cargo) refDataCollection
							.get(i);
					if (filterCargo != null
							&& filterCargo.getLkup_fld_id().equalsIgnoreCase(
									lkupFldId)) {
						filterCollection.add(filterCargo);
					}

				}

			} else {
				LKUP_GRP_Collection searchResults = adminSearchBO
						.loadReferenceTables(tableId);
				if (searchResults != null && searchResults.size() > 0) {
					LKUP_GRP_Cargo lkupCargo = (LKUP_GRP_Cargo) searchResults
							.get(0);
					refTableId = lkupCargo.getLkup_grp_cd();
					refTabDesc = lkupCargo.getLkup_grp_dsc();

				}
			}

			session.put(CafeConstants.REFERENCE_TABLE_DATA_COLLECTION, refDataCollection);

			ADReferenceDataListView listviewbean = new ADReferenceDataListView();

			listviewbean.setLanguage(langInd);
			// call the set DisplayData
			listviewbean.setDisplayData(filterCollection);
			// uniquely name the listview
			listviewbean.setName(AppConstants.ADReferenceDataListView_Listview);
			// this oen for the paging
			listviewbean.setPageSize(20);
			// define the columns to be sorted on takes a boolean array
			boolean sortableCols[] = { false, false, false, false, false };
			listviewbean.setSortableColumns(sortableCols);
			// specify the type of the columns in listview
			int[] dataType = { IListviewFormatter.stringCol,
					IListviewFormatter.stringCol };
			listviewbean.setColDataType(dataType);
			pageCollection
					.put(AppConstants.ADReferenceDataListView_Listview, listviewbean);
			pageCollection.put(CafeConstants.REF_TAB_ID, refTableId);
			pageCollection.put(CafeConstants.REF_TABLE_DESC, refTabDesc);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),CafeConstants.LOAD_REFERENCE_DATA,fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID(CafeConstants.LOAD_REFERENCE_DATA);
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					CafeConstants.LOAD_REFERENCE_DATA, e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::loadReferenceData::End");

	}

	/**
	 * This method is used to delete a selected row from the reference table.
	 * Updates the Active_flg to 'N'.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void storeReferenceData(FwTransaction txnBean)
			throws javax.ejb.EJBException {

		log(ILog.INFO, "AdminEJBBean::storeReferenceData::Start");
		CUSTOM_REFERENCE_TABLE_DATA_Collection refDataCollection = null;// Custom
		// Reference
		// Table
		// Data
		// Collection
		CUSTOM_REFERENCE_TABLE_DATA_Cargo refDataCargo = null; // Custom
		// Reference
		// Table Data
		// Cargo
		LKUP_Cargo lkupCargo = null; // Lookup Cargo
		LKUP_Collection lkupCollection = new LKUP_Collection(); // Lookup
		// Collection

		try {
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection	
		
			AdminSearchBO adminSearchBO = new AdminSearchBO();

			CUSTOM_REFERENCE_TABLE_DATA_Collection customRefDataColl = (CUSTOM_REFERENCE_TABLE_DATA_Collection) pageCollection
					.get(AppConstants.CUSTOM_REFERENCE_TABLE_DATA_Collection); // Custom
			// Reference
			// Table
			// Data
			// Collection
			CUSTOM_REFERENCE_TABLE_DATA_Cargo customRefDataCargo = customRefDataColl
					.getResult(0); // Custom Reference Table Data Cargo

			refDataCollection = adminSearchBO.loadReferenceDataForDelete(
					customRefDataCargo.getLkup_grp_cd().trim(),
					customRefDataCargo.getLkup_cd().trim());
			if (refDataCollection != null && refDataCollection.size() > 0) {

				int size = refDataCollection.size(); // Reference Data
				// Collection
				for (int i = 0; i < size; i++) {
					refDataCargo = (CUSTOM_REFERENCE_TABLE_DATA_Cargo) refDataCollection
							.get(i);

					lkupCargo = new LKUP_Cargo();

					if (refDataCargo.getLkup_cd().equalsIgnoreCase(
							customRefDataCargo.getLkup_cd())) {
						lkupCargo.setRowAction(FwConstants.ROWACTION_UPDATE);

						lkupCargo.setLkup_cd(refDataCargo.getLkup_cd().trim());
						lkupCargo.setLkup_grp_fld_id(refDataCargo
								.getLkup_grp_fld_id());
						lkupCargo.setCd_actv_flg("N");
						lkupCargo.setSort_ord(refDataCargo.getSort_ord());
						lkupCargo.setLkup_dsc(refDataCargo.getLkup_dsc());
						lkupCargo.setUpdt_dt(FwDate.getInstance().getDate()
								.toString());
						lkupCollection.add(lkupCargo);

					}
				}
			}

			adminSearchBO.storeReferenceTablesData(lkupCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"storeReferenceData",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("storeReferenceData");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"storeReferenceData", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::storeReferenceData::End");
	}

	/**
	 * This method loads the details of a particular row in the reference table
	 * for editing.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadReferenceRowData(FwTransaction txnBean)
			throws javax.ejb.EJBException {

		log(ILog.INFO, "AdminEJBBean::loadReferenceRowData::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map pageCollection = txnBean.getPageCollection(); // Page
		
			CUSTOM_REFERENCE_TABLE_DATA_Collection refDataCollection = null; // Custom
			// Reference
			// Table
			// Data
			// Collection
			CUSTOM_REFERENCE_TABLE_DATA_Collection refCollection = null; // Custom
			// Reference
			// Table
			// Data
			// Collection
			CUSTOM_REFERENCE_TABLE_DATA_Collection filterDataCollection = new CUSTOM_REFERENCE_TABLE_DATA_Collection(); // Custom
			// Reference
			// Table
			// Data
			// Collection
			CUSTOM_REFERENCE_TABLE_DATA_Cargo refDataCargo = null; // Custom
			// Reference
			// Table
			// Data
			// Cargo

			CUSTOM_REFERENCE_TABLE_DATA_Collection customRefDataColl = (CUSTOM_REFERENCE_TABLE_DATA_Collection) pageCollection
					.get(AppConstants.CUSTOM_REFERENCE_TABLE_DATA_Collection); // Custom
			// Reference
			// Table
			// Data
			// Collection
			CUSTOM_REFERENCE_TABLE_DATA_Cargo customRefDataCargo = customRefDataColl
					.getResult(0); // Custom Reference Table Data Cargo

			String refTableId = null; // reference table id
			String refTablename = null; // reference table name
			String refCode = null; // lookup code
			String activeCode = null; // activation code
//			String sort = null; // sort order
			AdminSearchBO adminSearchBO = new AdminSearchBO();

			refCollection = adminSearchBO
					.loadReferenceAddData(customRefDataCargo.getLkup_grp_cd()
							.trim());
			refDataCollection = adminSearchBO.loadReferenceDataForDelete(
					customRefDataCargo.getLkup_grp_cd().trim(),
					customRefDataCargo.getLkup_cd().trim());
			if(refDataCollection!=null && refDataCollection.size()>0){
				refTableId = refDataCollection.getResult(0).getLkup_grp_cd();
				refTablename = refDataCollection.getResult(0).getLkup_grp_dsc();
			}
			refCode = customRefDataCargo.getLkup_cd();

			for (int i = 0; refDataCollection != null
					&& i < refDataCollection.size(); i++) {
				refDataCargo = (CUSTOM_REFERENCE_TABLE_DATA_Cargo) refDataCollection
						.get(i);

				if (refDataCargo.getLkup_cd().trim().equalsIgnoreCase(
						customRefDataCargo.getLkup_cd().trim())) {
					activeCode = refDataCargo.getCd_actv_flg();
					filterDataCollection.add(refDataCargo);
				}
			}
			pageCollection.put(CafeConstants.REFERENCE_VALUES_COLLECTION, refCollection);
			session.put(CafeConstants.REFERENCE_VALUES_COLLECTION, refCollection);
			pageCollection.put(CafeConstants.REF_COLLECTION, filterDataCollection);
			session.put(CafeConstants.REF_COLLECTION, filterDataCollection);

			pageCollection.put(CafeConstants.REF_TAB_ID, refTableId);
			pageCollection.put(CafeConstants.REF_TABLE_DESC, refTablename);
			pageCollection.put("activeCode", activeCode);
			pageCollection.put("refCode", refCode);


		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"loadReferenceRowData",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("loadReferenceRowData");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"loadReferenceRowData", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::loadReferenceRowData::End");
	}
	/**
	 * This method is used to store a new/edited row into a reference table.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void storeReferenceRowData(FwTransaction txnBean)
			throws javax.ejb.EJBException {

		log(ILog.INFO, "AdminEJBBean::storeReferenceRowData::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			CUSTOM_REFERENCE_TABLE_DATA_Collection refDataCollection = null; // Custom
			// Reference
			// Table
			// Data
			// Collection
			CUSTOM_REFERENCE_TABLE_DATA_Collection filterDataCollection = new CUSTOM_REFERENCE_TABLE_DATA_Collection(); // Custom
			// Reference
			// Table
			// Data
			// Collection
			CUSTOM_REFERENCE_TABLE_DATA_Collection refCollection = new CUSTOM_REFERENCE_TABLE_DATA_Collection(); // Custom
			// Reference
			// Table
			// Data
			// Collection

			CUSTOM_REFERENCE_TABLE_DATA_Cargo refCargo = null; // Custom
			// Reference
			// Table Data
			// Cargo
			CUSTOM_REFERENCE_TABLE_DATA_Cargo refDataCargo = null; // Custom
			// Reference
			// Table
			// Data
			// Cargo
			CUSTOM_REFERENCE_TABLE_DATA_Cargo filterDataCargo = null;// Custom
			// Reference
			// Table
			// Data
			// Cargo

			LKUP_Cargo lkupCargo = null; // Lkup Cargo
			LKUP_Collection lkupCollection = new LKUP_Collection(); // Lkup
			// Collection
			String refCode = null; // Lookup Code
			String activationCode = null; // Activation Flag
			String action = null; // action
			String grpFldId = null; // Group Fld Id
			boolean editFlag = false; // boolean flag
			AdminSearchBO adminSearchBO = new AdminSearchBO();
			if (session.get(CafeConstants.REF_COLLECTION) != null) {
				refCollection = (CUSTOM_REFERENCE_TABLE_DATA_Collection) session
						.get(CafeConstants.REF_COLLECTION);
			}

			if (session.get(CafeConstants.REFERENCE_VALUES_COLLECTION) != null) {
				refDataCollection = (CUSTOM_REFERENCE_TABLE_DATA_Collection) session
						.get(CafeConstants.REFERENCE_VALUES_COLLECTION);

				activationCode = (String) request
						.get("referenceValues_activeCode");
				action = (String) request.get("method_action");
				refCode = (String) request.get("referenceValues_refCode");
				for (int i = 0; refDataCollection != null
						&& i < refDataCollection.size(); i++) {
					refDataCargo = (CUSTOM_REFERENCE_TABLE_DATA_Cargo) refDataCollection
							.get(i);

					grpFldId = (String) refDataCargo.getLkup_grp_fld_id();
					lkupCargo = new LKUP_Cargo();
					filterDataCargo = new CUSTOM_REFERENCE_TABLE_DATA_Cargo();
					if (action != null && action.equalsIgnoreCase("edit")) {
						editFlag = false;
						for (int j = 0; refCollection != null
								&& j < refCollection.size(); j++) {
							refCargo = (CUSTOM_REFERENCE_TABLE_DATA_Cargo) refCollection
									.get(j);
							if ((refCargo.getLkup_grp_fld_id()
									.equalsIgnoreCase(refDataCargo
											.getLkup_grp_fld_id()))
									&& (refCargo.getLkup_fld_id()
											.equalsIgnoreCase(refDataCargo
													.getLkup_fld_id()))) {
								editFlag = true;
								break;
							}

						}
						if (!editFlag) {
							lkupCargo
									.setRowAction(FwConstants.ROWACTION_INSERT);
						} else {
							lkupCargo
									.setRowAction(FwConstants.ROWACTION_UPDATE);
						}
					} else {
						lkupCargo.setRowAction(FwConstants.ROWACTION_INSERT);
					}

					lkupCargo.setCd_actv_flg(activationCode);

					lkupCargo.setUpdt_dt(FwDate.getInstance().getDate()
							.toString());

					lkupCargo.setLkup_cd(refCode);
					lkupCargo.setLkup_grp_fld_id(refDataCargo
							.getLkup_grp_fld_id());
					if (request.get(CafeConstants.REFERENCE_VALUES
							+ refDataCargo.getLang_cd() + "_"
							+ refDataCargo.getLkup_fld_dsc()) != null
							&& !request.get(
									CafeConstants.REFERENCE_VALUES
											+ refDataCargo.getLang_cd() + "_"
											+ refDataCargo.getLkup_fld_dsc())
									.equals("")) {
						lkupCargo.setLkup_dsc((String) request
								.get(CafeConstants.REFERENCE_VALUES
										+ refDataCargo.getLang_cd() + "_"
										+ refDataCargo.getLkup_fld_dsc()));
					} else {
						lkupCargo.setLkup_dsc(FwConstants.SPACE);
					}

					if (request.get(CafeConstants.REFERENCE_VALUES_SORT
							+ refDataCargo.getLang_cd() + "_"
							+ refDataCargo.getLkup_fld_dsc()) != null
							&& !request.get(
									CafeConstants.REFERENCE_VALUES_SORT
											+ refDataCargo.getLang_cd() + "_"
											+ refDataCargo.getLkup_fld_dsc())
									.equals("")) {

						lkupCargo.setSort_ord((String) request
								.get(CafeConstants.REFERENCE_VALUES_SORT
										+ refDataCargo.getLang_cd() + "_"
										+ refDataCargo.getLkup_fld_dsc()));

					} else {
						lkupCargo.setSort_ord("0");
					}
					lkupCollection.add(lkupCargo);

					filterDataCargo.setLkup_fld_dsc(refDataCargo
							.getLkup_fld_dsc());
					filterDataCargo.setLkup_dsc((String) request
							.get(CafeConstants.REFERENCE_VALUES + refDataCargo.getLang_cd()
									+ "_" + refDataCargo.getLkup_fld_dsc()));
					filterDataCargo.setSort_ord((String) request
							.get(CafeConstants.REFERENCE_VALUES_SORT
									+ refDataCargo.getLang_cd() + "_"
									+ refDataCargo.getLkup_fld_dsc()));
					filterDataCargo.setLang_cd(refDataCargo.getLang_cd());
					filterDataCargo.setLkup_grp_fld_id(lkupCargo
							.getLkup_grp_fld_id());
					filterDataCargo.setLkup_fld_id(refDataCargo
							.getLkup_fld_id());
					filterDataCollection.add(filterDataCargo);

				}
				ADRSRValidator adrsrValidator = new ADRSRValidator();
				if (action != null && action.equalsIgnoreCase("add")) {
					String maxNum = adminSearchBO
							.getLKUPData(refCode, grpFldId);
					if (maxNum != null && !maxNum.equalsIgnoreCase("0")) {
						adrsrValidator.validateLkupCd();
						if (adrsrValidator.hasMessages()) {
							request.put(FwConstants.MESSAGE_LIST,
									adrsrValidator.getMessageList());
							pageCollection.put("refDataCollection",
									filterDataCollection);
							pageCollection.put(CafeConstants.REF_TAB_ID, (String) request
									.get(CafeConstants.TABLE_ID));
							pageCollection.put(CafeConstants.REF_TABLE_DESC, (String) request
									.get(CafeConstants.TABLE_DESC));
							pageCollection.put("refCode", refCode);
							pageCollection.put("activeCode", activationCode);
							return;
						}

					}

				}
				adrsrValidator.validateRTData(filterDataCollection, refCode);
				if (adrsrValidator.hasMessages()) {
					request.put(FwConstants.MESSAGE_LIST, adrsrValidator
							.getMessageList());
					pageCollection.put("refDataCollection",
							filterDataCollection);
					if (action != null && action.equalsIgnoreCase("add")) {
						pageCollection.put(CafeConstants.REFERENCE_VALUES_COLLECTION,
								filterDataCollection);
						pageCollection.put(CafeConstants.REF_COLLECTION, refCollection);
					} else {
						pageCollection.put(CafeConstants.REFERENCE_VALUES_COLLECTION,
								refCollection);
						pageCollection.put(CafeConstants.REF_COLLECTION,
								filterDataCollection);

					}
					pageCollection.put(CafeConstants.REF_TAB_ID, (String) request
							.get(CafeConstants.TABLE_ID));
					pageCollection.put(CafeConstants.REF_TABLE_DESC, (String) request
							.get(CafeConstants.TABLE_DESC));
					pageCollection.put("refCode", refCode);
					pageCollection.put("activeCode", activationCode);
					return;
				}

			}

			adminSearchBO.storeReferenceTablesData(lkupCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"storeReferenceRowData",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("storeReferenceRowData");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"storeReferenceRowData", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::storeReferenceRowData::End");
	}

	/**
	 * This method is used to retrieve reference table columns for a given
	 * reference table while creating a new row of data.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadReferenceAddData(FwTransaction txnBean)
			throws javax.ejb.EJBException {

		log(ILog.INFO, "AdminEJBBean::loadReferenceAddData::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			CUSTOM_REFERENCE_TABLE_DATA_Collection refDataCollection = null; // Custom
			// Reference
			// Table
			// Data
			// Collection
			String refTabId = null; // Reference Table Id
			String refTabDesc = null; // Reference table Description
			AdminSearchBO adminSearchBO = new AdminSearchBO();

			refTabId = (String) request.get("reftabId");
			refTabDesc = (String) request.get("reftabDesc");

			refDataCollection = adminSearchBO.loadReferenceAddData(refTabId);

			pageCollection.put(CafeConstants.REF_TAB_ID, refTabId);
			pageCollection.put(CafeConstants.REF_TABLE_DESC, refTabDesc);
			pageCollection.put("refDataCollection", refDataCollection);
			session.put(CafeConstants.REFERENCE_VALUES_COLLECTION, refDataCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"loadReferenceAddData",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("loadReferenceAddData");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"loadReferenceAddData", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::loadReferenceAddData::End");
	}

	/**
	 * This method is used to retrieve reference table columns for
	 * creating/editing a reference table.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadRTAdd(FwTransaction txnBean) throws javax.ejb.EJBException {

		log(ILog.INFO, "AdminEJBBean::loadRTAdd::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			String tableId = "0"; // Reference table Id
			String tabDesc = null; // Reference Table Description
			AdminSearchBO adminSearchBO = new AdminSearchBO();
			Map fieldResults = null; // Map
			CUSTOM_REFERENCE_TABLE_DATA_Collection customRefDataColl = (CUSTOM_REFERENCE_TABLE_DATA_Collection) pageCollection
					.get(AppConstants.CUSTOM_REFERENCE_TABLE_DATA_Collection);
			if (customRefDataColl != null) {
				CUSTOM_REFERENCE_TABLE_DATA_Cargo customRefDataCargo = customRefDataColl
						.getResult(0);
				tableId = customRefDataCargo.getLkup_grp_cd();
				tabDesc = customRefDataCargo.getLkup_grp_dsc();
			}
			if (tableId != null && tableId.equalsIgnoreCase("0")) {
				if (pageCollection.get(CafeConstants.TABLE_ID) != null) {

					tableId = (String) pageCollection.get(CafeConstants.TABLE_ID);

				}
				if (pageCollection.get(CafeConstants.TABLE_DESC) != null) {

					tabDesc = (String) pageCollection.get(CafeConstants.TABLE_DESC);

				}

			}

			fieldResults = adminSearchBO.getDomainsForTable(tableId);

			pageCollection.put(CafeConstants.RESULTS_COLLECTION1, fieldResults
					.get(CafeConstants.RESULTS_COLLECTION1));
			pageCollection.put(CafeConstants.RESULTS_COLLECTION2, fieldResults
					.get(CafeConstants.RESULTS_COLLECTION2));
			pageCollection.put(CafeConstants.TABLE_ID, tableId);
			pageCollection.put(CafeConstants.TABLE_DESC, tabDesc);
			session.put(CafeConstants.RESULTS_COLLECTION2, fieldResults.get(CafeConstants.RESULTS_COLLECTION2));
			session.put(CafeConstants.RESULTS_COLLECTION1, fieldResults.get(CafeConstants.RESULTS_COLLECTION1));


		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"loadRTAdd",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("loadRTAdd");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"loadRTAdd", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::loadRTAdd::End");
	}

	/**
	 * This is method is used to store reference table columns to a reference
	 * table.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void storeRTTable(FwTransaction txnBean)
			throws javax.ejb.EJBException {

		log(ILog.INFO, "AdminEJBBean::storeRTTable::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			String methodName = null; // Method Name
			String[] domains = null; // Domains List

			AdminSearchBO adminSearchBO = new AdminSearchBO();		
			methodName = (String) request.get("method_name");
			domains = (String[]) request.get("referenceValues_selected");

			LKUP_GRP_Collection customRefDataColl = (LKUP_GRP_Collection) pageCollection
					.get(AppConstants.LKUP_GRP_Collection); // LKUP Group Collection
			LKUP_GRP_Cargo lkupGrpCargo = customRefDataColl.getResult(0); // LKUP
			// Group
			// Cargo

			LKUP_GRP_Collection lkupGrpCollection = new LKUP_GRP_Collection(); // LKUP
			// Group
			// Collection

			LKUP_GRP_FLD_Collection lkupGrpFldColl = new LKUP_GRP_FLD_Collection(); // LKUP
			// Field
			// Collection
			LKUP_GRP_FLD_Cargo lkupGrpFldCargo = null; // LKUP Filed Cargo
			
			
			LKUP_GRP_FLD_Collection  newlangGrpFldColl = null;
			LKUP_GRP_FLD_Cargo newLangGrpFldCargo = null; // LKUP Filed Cargo
			
			LKUP_FLD_Collection fldCollection = null; // LKUP Field Collection
			LKUP_FLD_Cargo fldCargo = null; // LKUP filed Cargo
			ADRSRValidator adrsrValidator = new ADRSRValidator();

			int maxNum = adminSearchBO.getLKUPGRPFLDMaxNum(FwConstants.ENGLISH);
			int grpFldNo = 0; // Group Field Number
			String domainName = null; // Column Name

			if (methodName != null && methodName.equalsIgnoreCase(CafeConstants.UPDATE)) { 
				lkupGrpCargo.setRowAction(FwConstants.ROWACTION_UPDATE);
				lkupGrpCargo.setLkup_grp_cd(lkupGrpCargo.getLkup_grp_cd()
						.toUpperCase());

				if (domains != null && domains.length > 3) {

					if (session.get(CafeConstants.RESULTS_COLLECTION2) != null) {
						fldCollection = (LKUP_FLD_Collection) session
								.get(CafeConstants.RESULTS_COLLECTION2);
					}
					for (int k = 0; k < domains.length; k++) {
						domainName = (String) domains[k];
						if (domainName != null
								&& !(domainName.equalsIgnoreCase("C")
										|| domainName.equalsIgnoreCase("A") || domainName
										.equalsIgnoreCase("S"))) {
							boolean flag = false; // boolean flag

							for (int i = 0; fldCollection != null
									&& i < fldCollection.size(); i++) {
								fldCargo = (LKUP_FLD_Cargo) fldCollection
										.get(i);
						
								if (fldCargo.getLkup_fld_id().equalsIgnoreCase(
										domainName)) {
									
									newlangGrpFldColl = adminSearchBO.getLkupFldsForNewlanguage(domainName,
															FwConstants.VIETNAMESE,lkupGrpCargo.getLkup_grp_cd());
									
									if(newlangGrpFldColl==null){
										
										int grpFldId = adminSearchBO.getLKUPGRPFLDForExisting(domainName,
												FwConstants.ENGLISH,lkupGrpCargo.getLkup_grp_cd());
										newLangGrpFldCargo = new LKUP_GRP_FLD_Cargo();
										newLangGrpFldCargo.setRowAction(FwConstants.ROWACTION_INSERT);									
										newLangGrpFldCargo.setLkup_grp_fld_id(""+ (grpFldId + 20000));
										newLangGrpFldCargo.setLkup_grp_cd(lkupGrpCargo.getLkup_grp_cd());
										newLangGrpFldCargo.setLkup_fld_id(domainName);
										newLangGrpFldCargo.setLang_cd(FwConstants.VIETNAMESE);
										lkupGrpFldColl.add(newLangGrpFldCargo);
									}
									
									flag = true;
									break;
								}
							}
							if (!flag) {
								grpFldNo = maxNum;
								// English Row
								lkupGrpFldCargo = new LKUP_GRP_FLD_Cargo();
								lkupGrpFldCargo
										.setRowAction(FwConstants.ROWACTION_INSERT);
								lkupGrpFldCargo.setLkup_grp_fld_id(""
										+ (grpFldNo + 1));
								lkupGrpFldCargo.setLang_cd(FwConstants.ENGLISH);
								lkupGrpFldCargo.setLkup_fld_id(domainName);
								lkupGrpFldCargo.setLkup_grp_cd(lkupGrpCargo
										.getLkup_grp_cd());
								lkupGrpFldColl.add(lkupGrpFldCargo);
								
								// Spanish Row
								lkupGrpFldCargo = new LKUP_GRP_FLD_Cargo();
								lkupGrpFldCargo
										.setRowAction(FwConstants.ROWACTION_INSERT);
								lkupGrpFldCargo.setLkup_grp_fld_id(""
										+ (grpFldNo + 10000 + 1));
								lkupGrpFldCargo.setLang_cd(FwConstants.SPANISH);
								lkupGrpFldCargo.setLkup_fld_id(domainName);
								lkupGrpFldCargo.setLkup_grp_cd(lkupGrpCargo
										.getLkup_grp_cd());
								lkupGrpFldColl.add(lkupGrpFldCargo);
								
								// Vietnamese Row
								lkupGrpFldCargo = new LKUP_GRP_FLD_Cargo();
								lkupGrpFldCargo
										.setRowAction(FwConstants.ROWACTION_INSERT);
								lkupGrpFldCargo.setLkup_grp_fld_id(""
										+ (grpFldNo + 20000 + 1));
								lkupGrpFldCargo.setLang_cd(FwConstants.VIETNAMESE);
								lkupGrpFldCargo.setLkup_fld_id(domainName);
								lkupGrpFldCargo.setLkup_grp_cd(lkupGrpCargo
										.getLkup_grp_cd());								
								
								lkupGrpFldColl.add(lkupGrpFldCargo);

								maxNum = maxNum + 1;

							}

						}
					}

				}

			} else {
				lkupGrpCargo.setRowAction(FwConstants.ROWACTION_INSERT);
				lkupGrpCargo.setLkup_grp_cd(lkupGrpCargo.getLkup_grp_cd()
						.toUpperCase());

				if (domains != null && domains.length > 3) {
					for (int i = 0; i < domains.length; i++) {
						domainName = (String) domains[i];

						if (domainName != null
								&& !(domainName.equalsIgnoreCase("C")
										|| domainName.equalsIgnoreCase("A") || domainName
										.equalsIgnoreCase("S"))) {
							grpFldNo = adminSearchBO.checkNextSeries(maxNum);

							// English Row
							lkupGrpFldCargo = new LKUP_GRP_FLD_Cargo();
							lkupGrpFldCargo.setLkup_grp_fld_id(""
									+ (grpFldNo + 1));
							lkupGrpFldCargo.setLang_cd(FwConstants.ENGLISH);
							lkupGrpFldCargo.setLkup_fld_id(domainName);
							lkupGrpFldCargo.setLkup_grp_cd(lkupGrpCargo
									.getLkup_grp_cd());
							lkupGrpFldCargo
									.setRowAction(FwConstants.ROWACTION_INSERT);
							lkupGrpFldColl.add(lkupGrpFldCargo);

							// Spanish Row
							lkupGrpFldCargo = new LKUP_GRP_FLD_Cargo();
							lkupGrpFldCargo.setLkup_grp_fld_id(""
									+ (grpFldNo + 10000 + 1));
							lkupGrpFldCargo.setLang_cd(FwConstants.SPANISH);
							lkupGrpFldCargo.setLkup_fld_id(domainName);
							lkupGrpFldCargo.setLkup_grp_cd(lkupGrpCargo
									.getLkup_grp_cd());
							lkupGrpFldCargo
									.setRowAction(FwConstants.ROWACTION_INSERT);
							lkupGrpFldColl.add(lkupGrpFldCargo);
							
							// Vietnam Row
							lkupGrpFldCargo = new LKUP_GRP_FLD_Cargo();
							lkupGrpFldCargo.setLkup_grp_fld_id(""
									+ (grpFldNo + 20000 + 1));
							lkupGrpFldCargo.setLang_cd(FwConstants.VIETNAMESE);
							lkupGrpFldCargo.setLkup_fld_id(domainName);
							lkupGrpFldCargo.setLkup_grp_cd(lkupGrpCargo
									.getLkup_grp_cd());
							lkupGrpFldCargo
									.setRowAction(FwConstants.ROWACTION_INSERT);
							lkupGrpFldColl.add(lkupGrpFldCargo);
							
							maxNum = maxNum + 1;
						}

					}

				}

				LKUP_GRP_Collection searchResults = adminSearchBO
						.loadReferenceTables(lkupGrpCargo.getLkup_grp_cd());

				if (searchResults != null && searchResults.size() > 0) {
					adrsrValidator.validateRTName(searchResults);
					if (adrsrValidator.hasMessages()) {
						request.put(FwConstants.MESSAGE_LIST, adrsrValidator
								.getMessageList());
						pageCollection.put(CafeConstants.TABLE_ID, lkupGrpCargo
								.getLkup_grp_cd());
						pageCollection.put(CafeConstants.TABLE_DESC, lkupGrpCargo
								.getLkup_grp_dsc());
						pageCollection.put("methodName", "add");
						pageCollection.put(CafeConstants.RESULTS_COLLECTION1, session
								.get(CafeConstants.RESULTS_COLLECTION1));
						pageCollection.put(CafeConstants.RESULTS_COLLECTION2, session
								.get(CafeConstants.RESULTS_COLLECTION2));
						return;
					}

				}

			}

			lkupGrpCollection.add(lkupGrpCargo);

			adminSearchBO.storeRTTable(lkupGrpCollection, lkupGrpFldColl);
			session.remove(CafeConstants.RESULTS_COLLECTION1);
			session.remove(CafeConstants.RESULTS_COLLECTION2);
			pageCollection.put(CafeConstants.TABLE_ID, lkupGrpCargo.getLkup_grp_cd());
			pageCollection.put(CafeConstants.TABLE_DESC, lkupGrpCargo.getLkup_grp_dsc());


		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"storeRTTable",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("storeRTTable");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"storeRTTable", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::storeRTTable::End");
	}

	/**
	 * This method is used to retrieve all the messages (error/warning messages)
	 * for the given criteria (message id or message desc).
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void displayMSGData(FwTransaction txnBean)
			throws javax.ejb.EJBException {

		log(ILog.INFO, "AdminEJBBean::displayMSGData::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			String langInd = FwConstants.ENGLISH; // Language Indicator

			String msgId = (String) request.get("referenceValues_msgNo"); // Message
			// Id
			String msgDesc = (String) request.get("referenceValues_msgDesc");// Message
			// Description

			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				langInd = (String) httpSession.get(AppConstants.LANGUAGE);
			}

			ADMSGValidator admsgValidator = new ADMSGValidator();
			admsgValidator.validateMSGData(msgId);
			if (admsgValidator.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, admsgValidator
						.getMessageList());
				return;
			}

			AdminSearchBO adminSearchBO = new AdminSearchBO();

			MSG_Collection searchResults = adminSearchBO.displayMSGData(msgId,
					msgDesc); // Message Collection
			ADSearchMsgListView listviewbean = new ADSearchMsgListView();
			listviewbean.setLanguage(langInd);
			// call the set DisplayData
			listviewbean.setDisplayData(searchResults);
			// uniquely name the listview
			listviewbean.setName(AppConstants.ADSearchMsgListView_Listview);
			// this oen for the paging
			listviewbean.setPageSize(20);
			// define the columns to be sorted on takes a boolean array
			boolean sortableCols[] = { false, false, false };
			listviewbean.setSortableColumns(sortableCols);
			// specify the type of the columns in listview
			int[] dataType = { IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol };
			listviewbean.setColDataType(dataType);
			pageCollection.put(AppConstants.ADSearchMsgListView_Listview, listviewbean);
			pageCollection.put(CafeConstants.MSG_COLLECTION, searchResults);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"displayMSGData",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("displayMSGData");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"displayMSGData", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::displayMSGData::End");
	}

	/**
	 * This method is used to load message detials for a given msg_id for
	 * editing.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadMSGData(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::loadMSGData::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			MSG_Cargo msgCargo = null;// Message Cargo
			MSG_Cargo cargo = null;// Message Cargo
			MSG_Collection searchResult = null; // Message Collection
			MSG_Collection messageCollection = null; // Message Collection
			MSG_Collection msgCollection = (MSG_Collection) pageCollection
					.get(AppConstants.MSG_Collection); // Message Collection

			if (session.get(FwConstants.BEFORE_COLLECTION) != null) {
				Map pCollection = (Map) session
						.get(FwConstants.BEFORE_COLLECTION);

				if (pCollection.get(CafeConstants.MSG_COLLECTION) != null) {

					searchResult = (MSG_Collection) pCollection
							.get(CafeConstants.MSG_COLLECTION);

				}
			}

			if (msgCollection != null && msgCollection.size() > 0) {
				msgCargo = (MSG_Cargo) msgCollection.getResult(0);

				messageCollection = new MSG_Collection();

				for (int i = 0; searchResult != null && i < searchResult.size(); i++) {

					cargo = (MSG_Cargo) searchResult.get(i);
					if (msgCargo.getMsg_id() != null
							&& !(msgCargo.getMsg_id().equals(""))
							&& msgCargo.getMsg_id().equals(cargo.getMsg_id())) {
						messageCollection.add(cargo);
					}

				}

			}

			pageCollection.put(CafeConstants.MESSAGE_COLLECTION, messageCollection);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"loadMSGData",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("loadMSGData");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"loadMSGData", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::loadMSGData::End");
	}

	/**
	 * This method stores the edited message details to the table.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void storeMSGData(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::storeMSGData::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			String langInd = FwConstants.ENGLISH; // Language Indicator
			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				langInd = (String) httpSession.get(AppConstants.LANGUAGE);
			}

			String messageId = (String) request
					.get("referenceValues_messageId"); // message id
			String action = (String) request.get(CafeConstants.ACTION); // action
			String enDesc = (String) request
					.get("referenceValues_messageDesc_en"); // english
			// description
			String esDesc = (String) request
					.get("referenceValues_messageDesc_es"); // spanish
			// description
			String vtDesc = (String) request
			.get("referenceValues_messageDesc_vt"); // vietnamese description
			
			String severity = (String) request.get("referenceValues_severity"); // severity
			// type
			String application = (String) request
					.get("referenceValues_subsystem"); // Module Name
			String langCd = null; // Language Cd
			String msgId = null; // Message Id

			MSG_Collection msgCollection = new MSG_Collection();// Message
			// Collection
			MSG_Collection messageCollection = null; // Message Collection
			MSG_Cargo enCargo = new MSG_Cargo(); // Message Cargo
			MSG_Cargo esCargo = new MSG_Cargo(); // Message Cargo
			MSG_Cargo vtCargo = new MSG_Cargo(); // Message Cargo
			MSG_Cargo cargo = null; // Message Cargo
			AdminSearchBO adminBO = new AdminSearchBO();

			enCargo.setRowAction(FwConstants.ROWACTION_INSERT);
			enCargo.setMsg_id(messageId);
			if (enDesc == null || enDesc.length() == 0) {
				enCargo.setEng_txt(FwConstants.SPACE);
			} else {
				enCargo.setEng_txt(enDesc);
			}
			enCargo.setLang_cd(FwConstants.ENGLISH);
			enCargo.setSvr_cd(severity);

			esCargo.setRowAction(FwConstants.ROWACTION_INSERT);
			esCargo.setMsg_id(messageId);
			if (esDesc == null || esDesc.length() == 0) {
				esCargo.setEng_txt(FwConstants.SPACE);
			} else {
				esCargo.setEng_txt(esDesc);
			}
			esCargo.setLang_cd(FwConstants.SPANISH);
			esCargo.setSvr_cd(severity);
			
			
			vtCargo.setRowAction(FwConstants.ROWACTION_INSERT);
			vtCargo.setMsg_id(messageId);
			if (vtDesc == null || vtDesc.length() == 0) {
				vtCargo.setEng_txt(FwConstants.SPACE);
			} else {
				vtCargo.setEng_txt(vtDesc);
			}
			vtCargo.setLang_cd(FwConstants.VIETNAMESE);
			vtCargo.setSvr_cd(severity);			
			
			

			ADEMDValidator ademdValidator = new ADEMDValidator();
			ademdValidator.validateMsgData(enDesc, esDesc,vtDesc);
			if (ademdValidator.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, ademdValidator
						.getMessageList());
				msgCollection.add(enCargo);
				msgCollection.add(esCargo);
				msgCollection.add(vtCargo);
				pageCollection.put(CafeConstants.MESSAGE_COLLECTION, msgCollection);
				pageCollection.put(CafeConstants.APPLICATION, application);
				return;
			}

			if (action != null && action.equalsIgnoreCase(CafeConstants.UPDATE)) {

				if (session.get(FwConstants.BEFORE_COLLECTION) != null) {
					Map pCollection = (Map) session
							.get(FwConstants.BEFORE_COLLECTION);

					if (pCollection.get(CafeConstants.MESSAGE_COLLECTION) != null) {

						messageCollection = (MSG_Collection) pCollection
								.get(CafeConstants.MESSAGE_COLLECTION);

					}
				}

				if (messageCollection != null && messageCollection.size() > 0) {

					int size = messageCollection.size();
					for (int i = 0; i < size; i++) {
						cargo = (MSG_Cargo) messageCollection.get(i);

						langCd = cargo.getLang_cd();
						msgId = cargo.getMsg_id();

						if (langCd != null && !langCd.equalsIgnoreCase("")
								&& langCd.equalsIgnoreCase(FwConstants.ENGLISH)
								&& msgId != null && !msgId.equalsIgnoreCase("")
								&& msgId.equalsIgnoreCase(messageId)) {
							enCargo.setRowAction(FwConstants.ROWACTION_UPDATE);
						}
						if (langCd != null && !langCd.equalsIgnoreCase("")
								&& langCd.equalsIgnoreCase(FwConstants.SPANISH)
								&& msgId != null && !msgId.equalsIgnoreCase("")
								&& msgId.equalsIgnoreCase(messageId)) {
							esCargo.setRowAction(FwConstants.ROWACTION_UPDATE);
						}
						if (langCd != null && !langCd.equalsIgnoreCase("")
								&& langCd.equalsIgnoreCase(FwConstants.VIETNAMESE)
								&& msgId != null && !msgId.equalsIgnoreCase("")
								&& msgId.equalsIgnoreCase(messageId)) {
							vtCargo.setRowAction(FwConstants.ROWACTION_UPDATE);
						}
					}
				}
			} else {

				String maxNum = adminBO.getMSGMaxNum(application); // message
				// max num

				enCargo.setMsg_id(String.valueOf(Integer.parseInt(maxNum) + 1));
				esCargo.setMsg_id(String.valueOf(Integer.parseInt(maxNum) + 1));
				vtCargo.setMsg_id(String.valueOf(Integer.parseInt(maxNum) + 1));

			}
			msgCollection.add(enCargo);
			msgCollection.add(esCargo);
			msgCollection.add(vtCargo);
			adminBO.storeMSGData(msgCollection);

			// If data persist is successful then displaylatest data in search
			// page.
			ADSearchMsgListView listviewbean = new ADSearchMsgListView();
			listviewbean.setLanguage(langInd);
			// call the set DisplayData
			listviewbean.setDisplayData(msgCollection);
			// uniquely name the listview
			listviewbean.setName(AppConstants.ADSearchMsgListView_Listview);
			// this oen for the paging
			listviewbean.setPageSize(20);
			// define the columns to be sorted on takes a boolean array
			boolean sortableCols[] = { false, false, false };
			listviewbean.setSortableColumns(sortableCols);
			// specify the type of the columns in listview
			int[] dataType = { IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol };
			listviewbean.setColDataType(dataType);
			pageCollection.put(AppConstants.ADSearchMsgListView_Listview, listviewbean);
			pageCollection.put(CafeConstants.MSG_COLLECTION, msgCollection);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"storeMSGData",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("storeMSGData");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"storeMSGData", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::storeMSGData::End");
	}

	/**
	 * This method is used to load all the display texts (from DPLY_TXT)
	 * matching the given criteria (text id or text desc).
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void displayDplyTxtData(FwTransaction txnBean)
			throws javax.ejb.EJBException {

		log(ILog.INFO, "AdminEJBBean::displayDplyTxtData::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			String langInd = FwConstants.ENGLISH; // Language Indicator

			String dplyId = (String) request.get("referenceValues_dplyNo");
			String dplyDesc = (String) request.get("referenceValues_dplyDesc");

			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				langInd = (String) httpSession.get(AppConstants.LANGUAGE);
			}

			ADDSPValidator addspValidator = new ADDSPValidator();
			addspValidator.validateDplyData(dplyId, dplyDesc);
			if (addspValidator.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, addspValidator
						.getMessageList());
				return;
			}

			AdminSearchBO adminSearchBO = new AdminSearchBO();

			DPLY_TXT_Collection searchResults = adminSearchBO
					.displayDplyTxtData(dplyId, dplyDesc);
			ADSearchDplyListView listviewbean = new ADSearchDplyListView();
			listviewbean.setLanguage(langInd);
			// call the set DisplayData
			listviewbean.setDisplayData(searchResults);
			// uniquely name the listview
			listviewbean.setName(AppConstants.ADSearchDplyListView_Listview);
			// this oen for the paging
			listviewbean.setPageSize(20);
			// define the columns to be sorted on takes a boolean array
			boolean sortableCols[] = { false, false, false };
			listviewbean.setSortableColumns(sortableCols);
			// specify the type of the columns in listview
			int[] dataType = { IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol };
			listviewbean.setColDataType(dataType);
			pageCollection.put(AppConstants.ADSearchDplyListView_Listview, listviewbean);
			pageCollection.put(CafeConstants.DPLY_TXT_COLLECTION, searchResults);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"displayDplyTxtData",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("displayDplyTxtData");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"displayDplyTxtData", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::displayDplyTxtData::End");
	}

	/**
	 * This method is used to load the text details for a given text id for
	 * editing.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadDplyTxtData(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::loadDplyTxtData::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			DPLY_TXT_Cargo dplyCargo = null; // dply text cargo
			DPLY_TXT_Cargo cargo = null; // dply text cargo
			DPLY_TXT_Collection searchResult = null; // dply text collection
			DPLY_TXT_Collection dplyTxtCollection = null; // dply text
			// collection
			DPLY_TXT_Collection dplyCollection = (DPLY_TXT_Collection) pageCollection
					.get(AppConstants.DPLY_TXT_Collection); // dply text collection

			if (session.get(FwConstants.BEFORE_COLLECTION) != null) {
				Map pCollection = (Map) session
						.get(FwConstants.BEFORE_COLLECTION);

				if (pCollection.get(CafeConstants.DPLY_TXT_COLLECTION) != null) {

					searchResult = (DPLY_TXT_Collection) pCollection
							.get(CafeConstants.DPLY_TXT_COLLECTION);

				}
			}

			if (dplyCollection != null && dplyCollection.size() > 0) {
				dplyCargo = (DPLY_TXT_Cargo) dplyCollection.getResult(0);

				dplyTxtCollection = new DPLY_TXT_Collection();

				for (int i = 0; searchResult != null && i < searchResult.size(); i++) {

					cargo = (DPLY_TXT_Cargo) searchResult.get(i);
					if (dplyCargo.getTxt_id() != null
							&& !(dplyCargo.getTxt_id().equals(""))
							&& dplyCargo.getTxt_id().equals(cargo.getTxt_id())) {
						dplyTxtCollection.add(cargo);
					}

				}

			}

			pageCollection.put(CafeConstants.DPLY_TX_COLLECTION, dplyTxtCollection);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"loadDplyTxtData",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("loadDplyTxtData");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"loadDplyTxtData", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::loadDplyTxtData::End");
	}

	/**
	 * This method stores the edited/added display texts.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void storeDplyTxtData(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::storeDplyTxtData::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			String langInd = FwConstants.ENGLISH; // Language Indicator
			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				langInd = (String) httpSession.get(AppConstants.LANGUAGE);
			}

			String txtId = (String) request.get("referenceValues_txtId");// dply
			// text
			// id
			String action = (String) request.get(CafeConstants.ACTION); // action
			String enDesc = (String) request.get("referenceValues_txtDesc_en"); // english
			// description
			String esDesc = (String) request.get("referenceValues_txtDesc_es"); // spanish
			// description
			String vtDesc = (String) request.get("referenceValues_txtDesc_vt"); // vietnamese			
			// description
			String cmtTxt = (String) request.get("referenceValues_comment"); // comment
			// text
			String application = (String) request
					.get("referenceValues_subsystem"); // module code
			String langCd = null; // language cd
			String dplyTxtId = null; // dply text id

			DPLY_TXT_Collection dplyTxtCollection = new DPLY_TXT_Collection();// dply
			// text
			// collection
			DPLY_TXT_Collection dplyCollection = null; // dply text collection
			DPLY_TXT_Cargo enCargo = new DPLY_TXT_Cargo(); // dply text cargo
			DPLY_TXT_Cargo esCargo = new DPLY_TXT_Cargo(); // dply text cargo
			DPLY_TXT_Cargo vtCargo = new DPLY_TXT_Cargo(); // dply text cargo
			DPLY_TXT_Cargo cargo = null; // dply text cargo
			AdminSearchBO adminBO = new AdminSearchBO();

			enCargo.setRowAction(FwConstants.ROWACTION_INSERT);
			enCargo.setTxt_id(txtId);
			if (enDesc == null || enDesc.length() == 0) {
				enCargo.setEng_txt(FwConstants.SPACE);
			} else {
				enCargo.setEng_txt(enDesc);
			}
			enCargo.setLang_cd(FwConstants.ENGLISH);

			if (cmtTxt == null || cmtTxt.length() == 0) {
				enCargo.setCmt_txt(FwConstants.SPACE);
			} else {
				enCargo.setCmt_txt(cmtTxt);
			}

			esCargo.setRowAction(FwConstants.ROWACTION_INSERT);
			esCargo.setTxt_id(txtId);
			if (esDesc == null || esDesc.length() == 0) {
				esCargo.setEng_txt(FwConstants.SPACE);
			} else {
				esCargo.setEng_txt(esDesc);
			}
			esCargo.setLang_cd(FwConstants.SPANISH);
			if (cmtTxt == null || cmtTxt.length() == 0) {
				esCargo.setCmt_txt(FwConstants.SPACE);
			} else {
				esCargo.setCmt_txt(cmtTxt);
			}
			
			vtCargo.setRowAction(FwConstants.ROWACTION_INSERT);
			vtCargo.setTxt_id(txtId);
			if (vtDesc == null || vtDesc.length() == 0) {
				vtCargo.setEng_txt(FwConstants.SPACE);
			} else {
				vtCargo.setEng_txt(vtDesc);
			}
			vtCargo.setLang_cd(FwConstants.VIETNAMESE);
			if (cmtTxt == null || cmtTxt.length() == 0) {
				vtCargo.setCmt_txt(FwConstants.SPACE);
			} else {
				vtCargo.setCmt_txt(cmtTxt);
			}			

			ADEDTValidator adedtValidator = new ADEDTValidator();
			adedtValidator.validateDplyData(enDesc, esDesc, vtDesc,cmtTxt);
			if (adedtValidator.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, adedtValidator
						.getMessageList());
				dplyTxtCollection.add(enCargo);
				dplyTxtCollection.add(esCargo);
				dplyTxtCollection.add(vtCargo);
				pageCollection.put(CafeConstants.DPLY_TX_COLLECTION, dplyTxtCollection);
				pageCollection.put(CafeConstants.APPLICATION, application);
				return;
			}

			if (action != null && action.equalsIgnoreCase(CafeConstants.UPDATE)) {

				if (session.get(FwConstants.BEFORE_COLLECTION) != null) {
					Map pCollection = (Map) session
							.get(FwConstants.BEFORE_COLLECTION);

					if (pCollection.get(CafeConstants.DPLY_TX_COLLECTION) != null) {

						dplyCollection = (DPLY_TXT_Collection) pCollection
								.get(CafeConstants.DPLY_TX_COLLECTION);

					}
				}

				if (dplyCollection != null && dplyCollection.size() > 0) {

					int size = dplyCollection.size();
					for (int i = 0; i < size; i++) {
						cargo = (DPLY_TXT_Cargo) dplyCollection.get(i);

						langCd = cargo.getLang_cd();
						dplyTxtId = cargo.getTxt_id();

						if (langCd != null && !langCd.equalsIgnoreCase("")
								&& langCd.equalsIgnoreCase(FwConstants.ENGLISH)
								&& dplyTxtId != null
								&& !dplyTxtId.equalsIgnoreCase("")
								&& dplyTxtId.equalsIgnoreCase(txtId)) {
							enCargo.setRowAction(FwConstants.ROWACTION_UPDATE);
						}
						if (langCd != null && !langCd.equalsIgnoreCase("")
								&& langCd.equalsIgnoreCase(FwConstants.SPANISH)
								&& dplyTxtId != null
								&& !dplyTxtId.equalsIgnoreCase("")
								&& dplyTxtId.equalsIgnoreCase(txtId)) {
							esCargo.setRowAction(FwConstants.ROWACTION_UPDATE);
						}
						if (langCd != null && !langCd.equalsIgnoreCase("")
								&& langCd.equalsIgnoreCase(FwConstants.VIETNAMESE)
								&& dplyTxtId != null
								&& !dplyTxtId.equalsIgnoreCase("")
								&& dplyTxtId.equalsIgnoreCase(txtId)) {
							vtCargo.setRowAction(FwConstants.ROWACTION_UPDATE);
						}
					}
				}
			} else {

				String maxNum = adminBO.getDplyTxtMaxNum(application); // dply
				// text
				// max
				// num

				enCargo.setTxt_id(String.valueOf(Integer.parseInt(maxNum) + 1));
				esCargo.setTxt_id(String.valueOf(Integer.parseInt(maxNum) + 1));
				vtCargo.setTxt_id(String.valueOf(Integer.parseInt(maxNum) + 1));

			}
			dplyTxtCollection.add(enCargo);
			dplyTxtCollection.add(esCargo);
			dplyTxtCollection.add(vtCargo);
			adminBO.storeDplyTxtData(dplyTxtCollection);
			
			//temporary fix remove later

			
			// If data persist is successful then displaylatest data in search
			// page.
			ADSearchDplyListView listviewbean = new ADSearchDplyListView();
			listviewbean.setLanguage(langInd);
			// call the set DisplayData
			listviewbean.setDisplayData(dplyTxtCollection);
			// uniquely name the listview
			listviewbean.setName(AppConstants.ADSearchDplyListView_Listview);
			// this oen for the paging
			listviewbean.setPageSize(20);
			// define the columns to be sorted on takes a boolean array
			boolean sortableCols[] = { false, false, false };
			listviewbean.setSortableColumns(sortableCols);
			// specify the type of the columns in listview
			int[] dataType = { IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol };
			listviewbean.setColDataType(dataType);
			pageCollection.put(AppConstants.ADSearchDplyListView_Listview, listviewbean);
			pageCollection.put(CafeConstants.DPLY_TXT_COLLECTION, dplyTxtCollection);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"storeDplyTxtData",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("storeDplyTxtData");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"storeDplyTxtData", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::storeDplyTxtData::End");
	}

	/**
	 * This method retrieves and displays all the Users that match the given
	 * criteria (Userid or Last name to First name).
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void searchUser(FwTransaction txnBean) throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::searchUser::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			String langInd = FwConstants.ENGLISH; // Language Indicator
			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				langInd = (String) httpSession.get(AppConstants.LANGUAGE);
			}

			String userId = (String) request.get("userID"); // user id
			String firstName = (String) request.get("firstName"); // first
			// name
			String lastName = (String) request.get("lastName"); // last name

			ADM_USER_Collection amdUserCollection = null; // Adm
			// User
			// Collection
			ADM_USER_Cargo admCargo = new ADM_USER_Cargo(); // admin user cargo
			AdminSearchBO adminBO = new AdminSearchBO();

			ADSFUValidator scsfuValidator = new ADSFUValidator();

			scsfuValidator.validateSearchData(userId, firstName, lastName);
			if (scsfuValidator.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, scsfuValidator
						.getMessageList());
				return;

			}

			admCargo.setUser_id(userId);
			admCargo.setFst_nam(firstName);
			admCargo.setLast_nam(lastName);

			amdUserCollection = adminBO.searchUser(admCargo);
			// More than 100 records warning message.
			if (adminBO.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, adminBO.getMessageList());
			}
			
			
			// If data persist is successful then displaylatest data in search
			// page.
			ADSearchUserListView listviewbean = new ADSearchUserListView();
			listviewbean.setLanguage(langInd);
			// call the set DisplayData
			listviewbean.setDisplayData(amdUserCollection);
			// uniquely name the listview
			listviewbean.setName(AppConstants.ADSearchUserListView_Listview);
			// this oen for the paging
			listviewbean.setPageSize(20);
			// define the columns to be sorted on takes a boolean array
			boolean sortableCols[] = { false, false, false, false };
			listviewbean.setSortableColumns(sortableCols);
			// specify the type of the columns in listview
			int[] dataType = { IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol,
					IListviewFormatter.stringCol };
			listviewbean.setColDataType(dataType);
			pageCollection.put(AppConstants.ADSearchUserListView_Listview, listviewbean);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"searchUser",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("searchUser");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"searchUser", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::searchUser::End");
	}

	/**
	 * This method is used to store Roles for a given User
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void storeSecurityRoles(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::storeSecurityRoles::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
		
			String loginId = null; // login id
			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				Map securedSessMap = (Map) httpSession
						.get(FwConstants.SECURED_SESSION);
				if (securedSessMap != null) {
					loginId = (String) securedSessMap
							.get(FwConstants.LDAP_LOGON_ID);
				}
			}

			String roleName = (String) request.get("securityValues_roleName"); // role
			// name
			String roleDescription = (String) request
					.get("securityValues_roleDesc"); // role description

			ADM_ROLE_Collection admRoleCollection = new ADM_ROLE_Collection(); // admin
			// role
			// collection
			ADM_ROLE_Cargo admRoleCargo = new ADM_ROLE_Cargo(); // Admin role
			// cargo

			AdminSearchBO adminBO = new AdminSearchBO();

			admRoleCargo.setRowAction(FwConstants.ROWACTION_INSERT);
			admRoleCargo.setRole_desc(roleDescription);
			admRoleCargo.setRole_nam(roleName);
			admRoleCargo.setUpdated_by(loginId);
			admRoleCollection.add(admRoleCargo);

			ADCRRValidator sccrrValidator = new ADCRRValidator();

			sccrrValidator.validateRoleData(admRoleCargo);
			if (sccrrValidator.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, sccrrValidator
						.getMessageList());
				pageCollection.put("ADM_ROLE_SEARCH", admRoleCargo);
				return; // return to jsp
			}

			ADM_ROLE_Cargo admRoleValidateCargo = adminBO.checkRole(roleName);
			if (admRoleValidateCargo != null) {
				sccrrValidator.validateRoleName(admRoleValidateCargo);
				if (sccrrValidator.hasMessages()) {
					request.put(FwConstants.MESSAGE_LIST, sccrrValidator
							.getMessageList());
					pageCollection.put("ADM_ROLE_SEARCH", admRoleCargo);
					return; // return to jsp
				}
			}

			adminBO.storeSecurityRoles(admRoleCollection);
			sccrrValidator.roleNameSuccess();
			if (sccrrValidator.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, sccrrValidator
						.getMessageList());
			}

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"storeSecurityRoles",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("storeSecurityRoles");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"storeSecurityRoles", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::storeSecurityRoles::End");
	}

	/**
	 * This method is used to create new business functions (modules).
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void storeSecurityBusinessFunctions(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::storeSecurityBusinessFunctions::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			String loginId = null;// login id
			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				Map securedSessMap = (Map) httpSession
						.get(FwConstants.SECURED_SESSION);
				if (securedSessMap != null) {
					loginId = (String) securedSessMap
							.get(FwConstants.LDAP_LOGON_ID);
				}
			}

			String functionName = (String) request
					.get("securityValues_busFuncName");
			String functionDescription = (String) request
					.get("securityValues_busFuncDesc");

			ADM_BUS_FUNC_Collection admBusFuncCollection = new ADM_BUS_FUNC_Collection();// Admin
			// Business
			// Function
			// Collection
			ADM_BUS_FUNC_Cargo admBusFuncCargo = new ADM_BUS_FUNC_Cargo(); // Admin
			// Business
			// Function
			// Cargo

			AdminSearchBO adminBO = new AdminSearchBO();

			admBusFuncCargo.setRowAction(FwConstants.ROWACTION_INSERT);
			admBusFuncCargo.setBus_func_desc(functionDescription);
			admBusFuncCargo.setBus_func_nam(functionName);
			admBusFuncCargo.setUpdated_by(loginId);
			admBusFuncCollection.add(admBusFuncCargo);

			ADCBFValidator sccbfValidator = new ADCBFValidator();

			sccbfValidator.validateFunctionData(admBusFuncCargo);
			if (sccbfValidator.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, sccbfValidator
						.getMessageList());
				pageCollection.put("ADM_BUS_FUNC_SEARCH", admBusFuncCargo);
				return; // return to jsp
			}

			ADM_BUS_FUNC_Cargo admBusFuncValidateCargo = adminBO
					.checkBusinessFunction(functionName);
			if (admBusFuncValidateCargo != null) {
				sccbfValidator.validateFunctionName(admBusFuncValidateCargo);
				if (sccbfValidator.hasMessages()) {
					request.put(FwConstants.MESSAGE_LIST, sccbfValidator
							.getMessageList());
					pageCollection.put("ADM_BUS_FUNC_SEARCH", admBusFuncCargo);
					return; // return to jsp
				}
			}

			adminBO.storeSecurityBusinessFunctions(admBusFuncCollection);

			sccbfValidator.functionNameSuccess();
			if (sccbfValidator.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, sccbfValidator
						.getMessageList());
			}

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"storeSecurityBusinessFunctions",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("storeSecurityBusinessFunctions");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"storeSecurityBusinessFunctions", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::storeSecurityBusinessFunctions::End");
	}

	/**
	 * This method is used to load the existing business functions for
	 * maintaining the same.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadBusinessFunctions(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::loadBusinessFunctions::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			AdminSearchBO adminBO = new AdminSearchBO();
			ADM_BUS_FUNC_Collection busFuncCollection = null; // Admin
			// Business
			// Function
			// Collection

			busFuncCollection = adminBO.loadBusinessFunction();

			pageCollection.put(CafeConstants.BUS_FUNC_COLLECTION, busFuncCollection);
			session.put(CafeConstants.BUS_FUNC_COLLECTION, busFuncCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"loadBusinessFunctions",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("loadBusinessFunctions");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"loadBusinessFunctions", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::loadBusinessFunctions::End");
	}

	/**
	 * This method retrieves all the assigned and not yet assigned pages for a
	 * given business function (module);
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void searchPagesForBusinessFunctions(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::searchPagesForBusinessFunctions::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			Map fieldResults = null; // business function map
			String busFunc = (String) request.get("securityValues_busFunc");

			ADMBFValidator scmbfValidator = new ADMBFValidator();
			AdminSearchBO adminBO = new AdminSearchBO();
			scmbfValidator.validateBusinessFunction(busFunc);

			if (scmbfValidator.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, scmbfValidator
						.getMessageList());
				pageCollection.put(CafeConstants.BUS_FUNC_COLLECTION, session
						.get(CafeConstants.BUS_FUNC_COLLECTION));
				return; // return to jsp
			}

			fieldResults = adminBO.loadPageData(busFunc);

			pageCollection.put(CafeConstants.BUS_FUNC_COLL1, fieldResults
					.get(CafeConstants.BUS_FUNC_COLL1));
			pageCollection.put(CafeConstants.BUS_FUNC_COLL2, fieldResults
					.get(CafeConstants.BUS_FUNC_COLL2));
			pageCollection.put(CafeConstants.BUS_FUNC_COLLECTION, session
					.get(CafeConstants.BUS_FUNC_COLLECTION));
			pageCollection.put("busFunc_id", busFunc);
			pageCollection.put(CafeConstants.ACTION, "search");
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"searchPagesForBusinessFunctions",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("searchPagesForBusinessFunctions");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"searchPagesForBusinessFunctions", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::searchPagesForBusinessFunctions::End");
	}

	/**
	 * This method is used to store the newly added pages to a given business
	 * function.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void storeMaintainBusinessFunctions(FwTransaction txnBean)
			throws javax.ejb.EJBException {

		log(ILog.INFO, "AdminEJBBean::storeMaintainBusinessFunctions::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			String busFuncId = null; // business function id

			String loginId = null; // login id
			String[] domains = null; // selected pages
			Object domain = null; // domain Object

			AdminSearchBO adminSearchBO = new AdminSearchBO();

			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				Map securedSessMap = (Map) httpSession
						.get(FwConstants.SECURED_SESSION);
				if (securedSessMap != null) {
					loginId = (String) securedSessMap
							.get(FwConstants.LDAP_LOGON_ID);
				}
			}

			busFuncId = (String) request.get("securityValues_busFunc");
			domain = request.get("securityValues_selected");

			if (domain != null && domain instanceof String[]) {
				domains = (String[]) domain;
			} else {
				domains = new String[1];
				String selValue = (String) domain;
				domains[0] = selValue;
			}

			ADM_RES_ASSIG_Collection admResAssigCollection = new ADM_RES_ASSIG_Collection();
			PAGE_Collection maintainBusFunc_coll2 = null; // page collection
			PAGE_Cargo pageCargo = null; // page cargo
			ADM_RES_ASSIG_Cargo admResAssigCargo = null; // Admin Res Assign
			// Cargo

			if (session.get(FwConstants.BEFORE_COLLECTION) != null) {
				Map pCollection = (Map) session
						.get(FwConstants.BEFORE_COLLECTION);

				if (pCollection.get(CafeConstants.BUS_FUNC_COLL2) != null) {
					maintainBusFunc_coll2 = (PAGE_Collection) pCollection
							.get(CafeConstants.BUS_FUNC_COLL2);
				}
			}
			// Remove Old Records
			if (maintainBusFunc_coll2 != null
					&& maintainBusFunc_coll2.size() > 0) {

				int size = maintainBusFunc_coll2.size(); // business function
				// collection size
				for (int i = 0; i < size; i++) {
					pageCargo = (PAGE_Cargo) maintainBusFunc_coll2.get(i);

					admResAssigCargo = new ADM_RES_ASSIG_Cargo();
					admResAssigCargo.setRowAction(FwConstants.ROWACTION_DELETE);
					admResAssigCargo.setBus_func_id(busFuncId);
					admResAssigCargo.setPage_id(pageCargo.getPage_id());
					admResAssigCollection.add(admResAssigCargo);

				}
			}

			// Add New Records.
			if (domains != null && domains.length > 0) {

				int domainLength = domains.length; // domain length
				for (int k = 0; k < domainLength; k++) {
					admResAssigCargo = new ADM_RES_ASSIG_Cargo();
					admResAssigCargo.setRowAction(FwConstants.ROWACTION_INSERT);
					admResAssigCargo.setBus_func_id(busFuncId);
					admResAssigCargo.setPage_id((String) domains[k]);
					admResAssigCargo.setUpdated_by(loginId);
					admResAssigCollection.add(admResAssigCargo);
				}
			}
			adminSearchBO.storeMaintainBusinessFunctions(admResAssigCollection);
			pageCollection.put("RECORD_UPDATE", CafeConstants.TRUE);

			session.remove(FwConstants.BEFORE_COLLECTION);
			session.remove(CafeConstants.BUS_FUNC_COLLECTION);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"storeMaintainBusinessFunctions",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("storeMaintainBusinessFunctions");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"storeMaintainBusinessFunctions", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::storeMaintainBusinessFunctions::End");
	}

	/**
	 * This method retrieves the available security roles.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadSecurityRoles(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::loadSecurityRoles::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection

			AdminSearchBO adminBO = new AdminSearchBO();
			ADM_ROLE_Collection roleCollection = null; // Admin Role Collection

			roleCollection = adminBO.loadAdmRoles();

			pageCollection.put(CafeConstants.ROLE_COLLECTION, roleCollection);
			session.put(CafeConstants.ROLE_COLLECTION, roleCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"loadSecurityRoles",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("loadSecurityRoles");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"loadSecurityRoles", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::loadSecurityRoles::End");
	}

	/**
	 * This method retrieves assigned and unassigned business functions
	 * (modules) for a given security role.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void searchBusinessFunctionsForRole(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::searchBusinessFunctionsForRole::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			Map fieldResults = null; // Business Functions Map

			String role = (String) request.get("securityValues_role");

			ADMTRValidator scmtrValidator = new ADMTRValidator();
			AdminSearchBO adminBO = new AdminSearchBO();
			scmtrValidator.validateRole(role);

			if (scmtrValidator.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, scmtrValidator
						.getMessageList());
				pageCollection.put(CafeConstants.ROLE_COLLECTION, session
						.get(CafeConstants.ROLE_COLLECTION));
				return; // returns to the jsp
			}

			fieldResults = adminBO.loadBusinessFunctions(role);

			pageCollection.put(CafeConstants.ROLE_COLL1, fieldResults
					.get(CafeConstants.ROLE_COLL1));
			pageCollection.put(CafeConstants.ROLE_COLL2, fieldResults
					.get(CafeConstants.ROLE_COLL2));
			pageCollection.put(CafeConstants.ROLE_COLLECTION, session
					.get(CafeConstants.ROLE_COLLECTION));
			pageCollection.put("role_id", role);
			pageCollection.put(CafeConstants.ACTION, "search");
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"searchBusinessFunctionsForRole",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("searchBusinessFunctionsForRole");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"searchBusinessFunctionsForRole", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::searchBusinessFunctionsForRole::End");
	}

	/**
	 * This method is used to store the newly added business functions for a
	 * given security role.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void storeMaintainRoles(FwTransaction txnBean)
			throws javax.ejb.EJBException {

		log(ILog.INFO, "AdminEJBBean::storeMaintainRoles::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			String roleId = null; // role id

			String[] domains = null; // selected role
			Object domain = null; // Object
			String userId = null; // user id
			AdminSearchBO adminSearchBO = new AdminSearchBO();
			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				Map securedSessMap = (Map) httpSession
						.get(FwConstants.SECURED_SESSION);
				if (securedSessMap != null) {
					userId = (String) securedSessMap
							.get(FwConstants.LDAP_LOGON_ID);
				}
			}

			roleId = (String) request.get("securityValues_role");
			domain = request.get("securityValues_selected");

			if (domain != null && domain instanceof String[]) {
				domains = (String[]) domain;
			} else {
				domains = new String[1];
				String selValue = (String) domain;
				domains[0] = selValue;
			}

			ADM_BF_ASSIG_Collection admBfAssigCollection = new ADM_BF_ASSIG_Collection(); // Admin
			// Business
			// Function
			// Assig
			// Collection
			// Business
			// Function
			// Collection
			ADM_BUS_FUNC_Collection maintainRole_coll2 = null; // Admin
			// Business
			// Function
			// Collection
			ADM_BUS_FUNC_Cargo admBusFuncCargo = null; // Admin Business
			// Function Cargo
			ADM_BF_ASSIG_Cargo admBfAssigCargo = null; // Admin Business
			// Function Cargo

			if (session.get(FwConstants.BEFORE_COLLECTION) != null) {
				Map pCollection = (Map) session
						.get(FwConstants.BEFORE_COLLECTION);

				if (pCollection.get(CafeConstants.ROLE_COLL2) != null) {
					maintainRole_coll2 = (ADM_BUS_FUNC_Collection) pCollection
							.get(CafeConstants.ROLE_COLL2);
				}
			}

			// Remove Old Records
			if (maintainRole_coll2 != null && maintainRole_coll2.size() > 0) {

				int size = maintainRole_coll2.size();
				for (int i = 0; i < size; i++) {
					admBusFuncCargo = (ADM_BUS_FUNC_Cargo) maintainRole_coll2
							.get(i);

					admBfAssigCargo = new ADM_BF_ASSIG_Cargo();
					admBfAssigCargo.setRowAction(FwConstants.ROWACTION_DELETE);
					admBfAssigCargo.setRole_id(roleId);
					admBfAssigCargo.setBus_func_id(admBusFuncCargo
							.getBus_func_id());
					admBfAssigCollection.add(admBfAssigCargo);

				}
			}
			// Add New Records.
			if (domains != null && domains.length > 0) {
				int domainLength = domains.length;
				for (int k = 0; k < domainLength; k++) {
					admBfAssigCargo = new ADM_BF_ASSIG_Cargo();
					admBfAssigCargo.setRowAction(FwConstants.ROWACTION_INSERT);
					admBfAssigCargo.setRole_id(roleId);
					admBfAssigCargo.setBus_func_id((String) domains[k]);
					admBfAssigCargo.setUpdated_by(userId);
					admBfAssigCollection.add(admBfAssigCargo);
				}
			}

			adminSearchBO.storeMaintainRoles(admBfAssigCollection);

			pageCollection.put("RECORD_UPDATE", CafeConstants.TRUE);
			session.remove(FwConstants.BEFORE_COLLECTION);
			session.remove(CafeConstants.ROLE_COLLECTION);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"storeMaintainRoles",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("storeMaintainRoles");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"storeMaintainRoles", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::storeMaintainRoles::End");
	}

	/**
	 * This method is used to load assigned and unassigned security roles for a
	 * given User Id.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadAssignRoles(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::loadAssignRoles::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map pageCollection = txnBean.getPageCollection(); // Page
		

			ADM_USER_Collection admUserColl = (ADM_USER_Collection) pageCollection
					.get(AppConstants.ADM_USER_Collection);

			String userId = null; // user id
			String userName = null; // user name
			Map fieldResults = null;// roles map

			if (admUserColl != null && admUserColl.size() > 0) {

				ADM_USER_Cargo admUserCargo = (ADM_USER_Cargo) admUserColl
						.get(0);

				userId = admUserCargo.getUser_id();
				userName = admUserCargo.getFst_nam() + " "
						+ admUserCargo.getLast_nam();

			}

			AdminSearchBO adminBO = new AdminSearchBO();
			fieldResults = adminBO.loadRoles(userId);

			pageCollection.put(CafeConstants.USER_ROLL_COLL1, fieldResults
					.get(CafeConstants.USER_ROLL_COLL1));
			pageCollection.put(CafeConstants.USER_ROLL_COLL2, fieldResults
					.get(CafeConstants.USER_ROLL_COLL2));
			pageCollection.put("user_id", userId);
			pageCollection.put("user_name", userName);
			pageCollection.put(CafeConstants.ACTION, "search");
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"loadAssignRoles",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("loadAssignRoles");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"loadAssignRoles", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::loadAssignRoles::End");
	}

	/**
	 * This method is used to store the newly assigned security roles to a User
	 * id.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void storeAssignRoles(FwTransaction txnBean)
			throws javax.ejb.EJBException {

		log(ILog.INFO, "AdminEJBBean::storeAssignRoles::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			String userId = null;// user id
			String loginId = null;// login id

			String[] domains = null;// selected roles
			Object domain = null;// Object

			AdminSearchBO adminSearchBO = new AdminSearchBO();

			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				Map securedSessMap = (Map) httpSession
						.get(FwConstants.SECURED_SESSION);
				if (securedSessMap != null) {
					loginId = (String) securedSessMap
							.get(FwConstants.LDAP_LOGON_ID);
				}
			}

			userId = (String) request.get("userID");
			domain = request.get("securityValues_selected");

			if (domain != null && domain instanceof String[]) {
				domains = (String[]) domain;
			} else {
				domains = new String[1];
				String selValue = (String) domain;
				domains[0] = selValue;
			}

			ADM_USR_ASSIG_Collection admUsrAssigCollection = new ADM_USR_ASSIG_Collection(); // Admin
			// User
			// Assig
			// Collection
			// Collection
			ADM_ROLE_Collection maintainUserRole_coll2 = null; // Admin Role
			// Collection
			ADM_ROLE_Cargo admRoleCargo = null; // Admin Role Cargo
			ADM_USR_ASSIG_Cargo admUsrAssigCargo = null; // Admin User Assig
			// Cargo

			if (session.get(FwConstants.BEFORE_COLLECTION) != null) {
				Map pCollection = (Map) session
						.get(FwConstants.BEFORE_COLLECTION);
				if (pCollection.get(CafeConstants.USER_ROLL_COLL2) != null) {
					maintainUserRole_coll2 = (ADM_ROLE_Collection) pCollection
							.get(CafeConstants.USER_ROLL_COLL2);
				}
			}

			// Remove Old Records
			if (maintainUserRole_coll2 != null
					&& maintainUserRole_coll2.size() > 0) {
				int size = maintainUserRole_coll2.size();
				for (int i = 0; i < size; i++) {
					admRoleCargo = (ADM_ROLE_Cargo) maintainUserRole_coll2
							.get(i);

					admUsrAssigCargo = new ADM_USR_ASSIG_Cargo();
					admUsrAssigCargo.setRowAction(FwConstants.ROWACTION_DELETE);
					admUsrAssigCargo.setUser_id(userId);
					admUsrAssigCargo.setRole_id(admRoleCargo.getRole_id());
					admUsrAssigCollection.add(admUsrAssigCargo);

				}
			}

			// Add New Records.
			if (domains != null && domains.length > 0) {
				int domainLength = domains.length;
				for (int k = 0; k < domainLength; k++) {
					admUsrAssigCargo = new ADM_USR_ASSIG_Cargo();
					admUsrAssigCargo.setRowAction(FwConstants.ROWACTION_INSERT);
					admUsrAssigCargo.setUser_id(userId);
					admUsrAssigCargo.setRole_id((String) domains[k]);
					admUsrAssigCargo.setUpdated_by(loginId);
					admUsrAssigCargo.setEff_beg_dt(FwDate.getInstance()
							.getDate().toString());
					admUsrAssigCollection.add(admUsrAssigCargo);
				}
			}

			adminSearchBO.storeAssignRoles(admUsrAssigCollection);
			pageCollection.put("RECORD_UPDATE", CafeConstants.TRUE);
			session.remove(FwConstants.BEFORE_COLLECTION);
			session.remove(CafeConstants.ROLE_COLLECTION);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"storeAssignRoles",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("storeAssignRoles");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"storeAssignRoles", e);
			throw fe;
		}

		log(ILog.INFO, "AdminEJBBean::storeAssignRoles::End");
	}

	/**
	 * This method retrieves scheduled timings for all the batch processes.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadBatchData(FwTransaction txnBean)
			throws javax.ejb.EJBException {

		log(ILog.INFO, "AdminEJBBean::loadBatchData::Start");
		try {
			Map session = txnBean.getSession(); // Session Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection

			AdminSearchBO adminSearchBO = new AdminSearchBO();
			CUSTOM_REFERENCE_TABLE_DATA_Collection refTableCollection = new CUSTOM_REFERENCE_TABLE_DATA_Collection();// Custom
			// Reference
			// Table
			// Data
			// Collection
			CUSTOM_REFERENCE_TABLE_DATA_Collection batchrefCollection = new CUSTOM_REFERENCE_TABLE_DATA_Collection();// Custom
			// Reference
			// Table
			// Data
			// Collection
			CUSTOM_REFERENCE_TABLE_DATA_Collection batchTableCollection = new CUSTOM_REFERENCE_TABLE_DATA_Collection();// Custom
			// Reference
			// Table
			// Data
			// Collection

			Map batchMap = null; // Batch Map

			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				Map securedSessMap = (Map) httpSession
						.get(FwConstants.SECURED_SESSION);
			}



			batchMap = adminSearchBO.getBatchData();

			if (batchMap != null && batchMap.get(CafeConstants.REF_COLLECTION) != null) {
				refTableCollection = (CUSTOM_REFERENCE_TABLE_DATA_Collection) batchMap
						.get(CafeConstants.REF_COLLECTION);
			}
			if (batchMap != null && batchMap.get("batchRefCollection") != null) {
				batchrefCollection = (CUSTOM_REFERENCE_TABLE_DATA_Collection) batchMap
						.get("batchRefCollection");
			}
			if (batchMap != null
					&& batchMap.get("batchRefTableCollection") != null) {
				batchTableCollection = (CUSTOM_REFERENCE_TABLE_DATA_Collection) batchMap
						.get("batchRefTableCollection");
			}

			pageCollection.put(CafeConstants.BATCH_PROCESS_COLLECTION, refTableCollection);
			session.put(CafeConstants.BATCH_PROCESS_COLLECTION, refTableCollection);
			pageCollection.put("BATCH_PROCESS_DESCRIPTION", batchrefCollection);
			pageCollection.put("BATCH_PROCESS_DATA", batchTableCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"loadBatchData",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("loadBatchData");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"loadBatchData", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::loadBatchData::End");
	}

	/**
	 * This method is used to update the edited batch schedules.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void storeBatchData(FwTransaction txnBean)
			throws javax.ejb.EJBException {

		log(ILog.INFO, "AdminEJBBean::storeBatchData::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			// Collection
			CUSTOM_REFERENCE_TABLE_DATA_Collection refDataCollection = null; // Custom
			// Reference
			// Table
			// Data
			// Collection
			CUSTOM_REFERENCE_TABLE_DATA_Cargo refDataCargo = null; // Custom
			// Reference
			// Table
			// Data
			// Cargo

			LKUP_Cargo lkupCargo = null; // Lookup Cargo
			LKUP_Collection lkupCollection = new LKUP_Collection(); // Lookup
			// Collection

			AdminSearchBO adminSearchBO = new AdminSearchBO();
			if (session.get(CafeConstants.BATCH_PROCESS_COLLECTION) != null) {
				refDataCollection = (CUSTOM_REFERENCE_TABLE_DATA_Collection) session
						.get(CafeConstants.BATCH_PROCESS_COLLECTION);

				if (refDataCollection != null && refDataCollection.size() > 0) {
					int size = refDataCollection.size(); // refDataCollection
					// Size
					for (int i = 0; i < size; i++) {
						refDataCargo = (CUSTOM_REFERENCE_TABLE_DATA_Cargo) refDataCollection
								.get(i);
						if (refDataCargo.getLkup_fld_id().equalsIgnoreCase(
								"165")) {

							lkupCargo = new LKUP_Cargo();

							lkupCargo
									.setRowAction(FwConstants.ROWACTION_UPDATE);
							if (request.get(CafeConstants.REFERENCE_VALUES
									+ refDataCargo.getLkup_cd() + "_"
									+ refDataCargo.getLkup_fld_id()) != null
									&& !request.get(
											CafeConstants.REFERENCE_VALUES
													+ refDataCargo.getLkup_cd()
													+ "_"
													+ refDataCargo
															.getLkup_fld_id())
											.equals("")) {

								lkupCargo
										.setLkup_dsc((String) request
												.get(CafeConstants.REFERENCE_VALUES
														+ refDataCargo
																.getLkup_cd()
														+ "_"
														+ refDataCargo
																.getLkup_fld_id()));
							}
							lkupCargo.setUpdt_dt(FwDate.getInstance().getDate()
									.toString());
							lkupCargo.setLkup_cd(refDataCargo.getLkup_cd());
							lkupCargo.setLkup_grp_fld_id(refDataCargo
									.getLkup_grp_fld_id());
							lkupCargo.setCd_actv_flg(refDataCargo
									.getCd_actv_flg());
							lkupCargo.setSort_ord(refDataCargo.getSort_ord());

							lkupCollection.add(lkupCargo);
						}

					}
				}
			}

			adminSearchBO.storeReferenceTablesData(lkupCollection);

			session.remove(CafeConstants.BATCH_PROCESS_COLLECTION);


		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"storeBatchData",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("storeBatchData");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"storeBatchData", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::storeBatchData::End");
	}

	/**
	 * This method is used to check the server status after application is
	 * deployed.
	 * 
	 * @param txnBean
	 * @throws EJBException
	 */
	public void serverMonitor(FwTransaction txnBean) throws EJBException {
		log(ILog.INFO, "AdminEJBBean::serverMonitor::Start");
		Map pageCollection = (Map) txnBean.getPageCollection(); // Page
		// Collection
		// Object
		try {
			AdminSearchBO monitor = new AdminSearchBO();
			monitor.chkServerStatus();
			pageCollection.put("success", "Y");
			log(ILog.INFO, "AdminEJBBean::serverMonitor::End");
		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"serverMonitor",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("serverMonitor");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"serverMonitor", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::serverMonitor::End");
	}

	/**
	 * This method loads the Worker Inbox search results.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadWorkInboxResults(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::loadWorkInboxResults::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			Map beforeCollection = (Map) session
					.get(FwConstants.BEFORE_COLLECTION); // Before Collection
			String langInd = FwConstants.ENGLISH; // Language Indicator
			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				langInd = (String) httpSession.get(AppConstants.LANGUAGE);
			}
						
			String onInboxStr = (String) request.get(OnInbox);
			CUSTOM_ADMIN_WORKER_INBOX_Collection custInboxColl = new CUSTOM_ADMIN_WORKER_INBOX_Collection();
			CUSTOM_ADMIN_WORKER_INBOX_Cargo custInboxCargo = null;
						
			if(onInboxStr!=null){
				custInboxColl = (CUSTOM_ADMIN_WORKER_INBOX_Collection) pageCollection
						.get(AppConstants.CUSTOM_ADMIN_WORKER_INBOX_Collection);
	
				// Get criteria collection from before collection if this method is
				// executed after 'Update' action
				if (beforeCollection != null
						&& beforeCollection.get(AfterUpdate) != null) {
					custInboxColl = (CUSTOM_ADMIN_WORKER_INBOX_Collection) beforeCollection
							.get(AppConstants.CUSTOM_ADMIN_WORKER_INBOX_Collection);
					beforeCollection.remove(AfterUpdate);
				}
	
				custInboxCargo = custInboxColl.getResult(0);
				custInboxCargo.setSrc_ind(AppConstants.RMC_RQST_HIST_SU);
				custInboxCargo.setSsn_num(concatenateSsnNumber(request,
						CafeConstants.SOCIAL_SECURITY));				
			}else{
				// For loading the inbox with 'Received' change reports on first load.				
				custInboxCargo = new CUSTOM_ADMIN_WORKER_INBOX_Cargo();
				custInboxCargo.setCr_status(AppConstants.RMC_APPLICATION_RECEIVED);
				custInboxCargo.setSrc_ind(AppConstants.RMC_RQST_HIST_SU);
				custInboxColl.addCargo(custInboxCargo);				
			}
			// PCR#405 - set status field to 'received' status on initial load.
			pageCollection.put(AppConstants.CUSTOM_ADMIN_WORKER_INBOX_Collection,
					custInboxColl);
			ADIBXValidator adibxValidator = new ADIBXValidator();
			adibxValidator.validateWorkerInboxCriteria(custInboxCargo);
			if (adibxValidator.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, adibxValidator
						.getMessageList());
				return;
			}

			AdminSearchBO adminBO = new AdminSearchBO();

			CUSTOM_ADMIN_WORKER_INBOX_Collection custInboxResultsColl = adminBO
					.getWorkerInboxResults(custInboxColl);
			// More than 100 records warning message.
			if (adminBO.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, adminBO.getMessageList());
			}
			String tabId = (String)request.get(FwConstants.TAB_ID);
			ADWorkerInboxResultsListView listviewbean = new ADWorkerInboxResultsListView();
			listviewbean.setLanguage(langInd);
			listviewbean.setTabId(tabId);
			listviewbean.setDisplayData(custInboxResultsColl);
			listviewbean.setName(AppConstants.ADWorkerInboxResultsListView);
			// setting page size
			listviewbean.setPageSize(20);
			// define the columns to be sorted on takes a boolean array
			boolean sortableCols[] = { false, false, false, false, false, false, 
					false, false, false, false, false , false};
			listviewbean.setSortableColumns(sortableCols);
			// specify the type of the columns in listview
			int[] dataType = { IListviewFormatter.stringCol,IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol };
			listviewbean.setColDataType(dataType);
			session.put(AppConstants.APP_NUM_LIST, listviewbean.getAppNumList());
			pageCollection.put(AppConstants.ADWorkerInboxResultsListView, listviewbean);

			pageCollection.put(AppConstants.CUSTOM_ADMIN_WORKER_INBOX_Results_Collection,
					custInboxResultsColl);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"loadWorkInboxResults",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("loadWorkInboxResults");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"loadWorkInboxResults", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::loadWorkInboxResults::End");
	}

	/**
	 * This method stores the status changes made in Worker Inbox search
	 * results.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void storeWorkInboxStatus(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::storeWorkInboxStatus::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			Map beforeCollection = (Map) session
					.get(FwConstants.BEFORE_COLLECTION); // Before Collection
			String userId = CafeConstants.APPLICATION;
			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				Map securedMap = (Map) httpSession
						.get(FwConstants.SECURED_SESSION);
				if (securedMap != null
						&& securedMap.get(FwConstants.LDAP_LOGON_ID) != null) {
					userId = (String) securedMap
							.get(FwConstants.LDAP_LOGON_ID);
				}
			}

			CUSTOM_ADMIN_WORKER_INBOX_Collection custInboxUpdateColl = (CUSTOM_ADMIN_WORKER_INBOX_Collection) pageCollection
					.get(AppConstants.CUSTOM_ADMIN_WORKER_INBOX_Collection);
			CUSTOM_ADMIN_WORKER_INBOX_Collection custInboxBeforeColl = (CUSTOM_ADMIN_WORKER_INBOX_Collection) beforeCollection
					.get(AppConstants.CUSTOM_ADMIN_WORKER_INBOX_Results_Collection);
			CUSTOM_ADMIN_WORKER_INBOX_Collection custInboxCriteriaColl = (CUSTOM_ADMIN_WORKER_INBOX_Collection) beforeCollection
					.get(AppConstants.CUSTOM_ADMIN_WORKER_INBOX_Collection);

			CUSTOM_ADMIN_WORKER_INBOX_Cargo custInboxCargo = custInboxCriteriaColl
					.getResult(0);

			custInboxCargo.setSsn_num(concatenateSsnNumber(request,
					CafeConstants.SOCIAL_SECURITY));

			int updatedSize = custInboxUpdateColl.size();
			int beforeSize = custInboxBeforeColl.size();
			CUSTOM_ADMIN_WORKER_INBOX_Cargo custInboxUpdateCargo = null;
			CUSTOM_ADMIN_WORKER_INBOX_Cargo custInboxBeforeCargo = null;
			RMC_RQST_HIST_Collection rmcRqstColl = new RMC_RQST_HIST_Collection();
			RMC_RQST_HIST_Cargo rmcRqstCargo = null;

			for (int i = 0; i < updatedSize; i++) {
				custInboxUpdateCargo = custInboxUpdateColl.getResult(i);
				for (int j = 0; j < beforeSize; j++) {
					custInboxBeforeCargo = custInboxBeforeColl.getResult(j);
					if (custInboxBeforeCargo.getCr_num().equals(
							custInboxUpdateCargo.getCr_num())
							&& !custInboxBeforeCargo.getCr_status().equals(
									custInboxUpdateCargo.getCr_status())) {
						rmcRqstCargo = new RMC_RQST_HIST_Cargo();
						rmcRqstCargo.setApp_num(custInboxUpdateCargo
								.getCr_num());
						rmcRqstCargo.setSrc_ind(AppConstants.RMC_RQST_HIST_SU);
						rmcRqstCargo.setApp_stat_cd(custInboxUpdateCargo
								.getCr_status());
						rmcRqstCargo.setUpdt_by(userId);
						rmcRqstCargo.setUpdt_last_tms(fwDate
								.getFormattedDateTime());
						rmcRqstCargo.setRowAction(FwConstants.ROWACTION_INSERT);
						rmcRqstColl.addCargo(rmcRqstCargo);
					}
				}
			}

			pageCollection.put(AfterUpdate, AppConstants.YES);
			pageCollection.put(
					AppConstants.CUSTOM_ADMIN_WORKER_INBOX_Collection,
					custInboxCriteriaColl);

			ADIBXValidator adibxValidator = new ADIBXValidator();
			adibxValidator.validateWorkerInboxUpdate(rmcRqstColl);
			if (adibxValidator.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, adibxValidator
						.getMessageList());
				pageCollection.put(AppConstants.ADWorkerInboxResultsListView,
						beforeCollection.get(AppConstants.ADWorkerInboxResultsListView));
				return;
			}

			AdminSearchBO adminBO = new AdminSearchBO();
			adminBO.storeWorkInboxStatus(rmcRqstColl);

			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"storeWorkInboxStatus",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("storeWorkInboxStatus");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"storeWorkInboxStatus", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::storeWorkInboxStatus::End");
	}

	
	
	/**
	 * This method loads the Worker Inbox search results.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadCCWorkInboxResults(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::loadCCWorkInboxResults::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			Map beforeCollection = (Map) session
					.get(FwConstants.BEFORE_COLLECTION); // Before Collection
			String langInd = FwConstants.ENGLISH; // Language Indicator
			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				langInd = (String) httpSession.get(AppConstants.LANGUAGE);
			}
						
			String onInboxStr = (String) request.get(OnInbox);
			CUSTOM_ADMIN_WORKER_INBOX_Collection custInboxColl = new CUSTOM_ADMIN_WORKER_INBOX_Collection();
			CUSTOM_ADMIN_WORKER_INBOX_Cargo custInboxCargo = null;
						
			if(onInboxStr!=null){
				custInboxColl = (CUSTOM_ADMIN_WORKER_INBOX_Collection) pageCollection
						.get(AppConstants.CUSTOM_ADMIN_WORKER_INBOX_Collection);
	
				// Get criteria collection from before collection if this method is
				// executed after 'Update' action
				if (beforeCollection != null
						&& beforeCollection.get(AfterUpdate) != null) {
					custInboxColl = (CUSTOM_ADMIN_WORKER_INBOX_Collection) beforeCollection
							.get(AppConstants.CUSTOM_ADMIN_WORKER_INBOX_Collection);
					beforeCollection.remove(AfterUpdate);
				}
	
				custInboxCargo = custInboxColl.getResult(0);
				custInboxCargo.setSrc_ind(AppConstants.RMC_RQST_HIST_CC);
				custInboxCargo.setSsn_num(concatenateSsnNumber(request,
						CafeConstants.SOCIAL_SECURITY));				
			}else{
				// For loading the inbox with 'Received' change reports on first load.				
				custInboxCargo = new CUSTOM_ADMIN_WORKER_INBOX_Cargo();
				custInboxCargo.setCr_status(AppConstants.RMC_APPLICATION_RECEIVED);
				custInboxCargo.setSrc_ind(AppConstants.RMC_RQST_HIST_CC);
				custInboxColl.addCargo(custInboxCargo);				
			}
			// PCR#405 - set status field to 'received' status on initial load.
			pageCollection.put(AppConstants.CUSTOM_ADMIN_WORKER_INBOX_Collection,
					custInboxColl);
			ADCIBValidator adcibValidator = new ADCIBValidator();
			adcibValidator.validateWorkerInboxCriteria(custInboxCargo);
			if (adcibValidator.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, adcibValidator
						.getMessageList());
				return;
			}

			AdminSearchBO adminBO = new AdminSearchBO();

			CUSTOM_ADMIN_WORKER_INBOX_Collection custInboxResultsColl = adminBO
					.getWorkerInboxResults(custInboxColl);
			// More than 100 records warning message.
			if (adminBO.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, adminBO.getMessageList());
			}
			String tabId = (String)request.get(FwConstants.TAB_ID);
			ADCCWorkerInboxResultsListView listviewbean = new ADCCWorkerInboxResultsListView();
			listviewbean.setLanguage(langInd);
			listviewbean.setTabId(tabId);
			listviewbean.setDisplayData(custInboxResultsColl);
			listviewbean.setName(AppConstants.ADCCWorkerInboxResultsListView);
			// setting page size
			listviewbean.setPageSize(20);
			// define the columns to be sorted on takes a boolean array
			boolean sortableCols[] = { false, false, false, false, false, false, 
					false, false, false, false};
			listviewbean.setSortableColumns(sortableCols);
			// specify the type of the columns in listview
			int[] dataType = { IListviewFormatter.stringCol,IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol};
			listviewbean.setColDataType(dataType);
			session.put(AppConstants.APP_NUM_LIST, listviewbean.getAppNumList());
			pageCollection.put(AppConstants.ADCCWorkerInboxResultsListView, listviewbean);

			pageCollection.put(AppConstants.CUSTOM_ADMIN_WORKER_INBOX_Results_Collection,
					custInboxResultsColl);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"loadCCWorkInboxResults",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("loadCCWorkInboxResults");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"loadCCWorkInboxResults", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::loadCCWorkInboxResults::End");
	}

	/**
	 * This method stores the status changes made in Worker Inbox search
	 * results.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void storeCCWorkInboxStatus(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::storeCCWorkInboxStatus::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			Map beforeCollection = (Map) session
					.get(FwConstants.BEFORE_COLLECTION); // Before Collection
			String userId = CafeConstants.APPLICATION;
			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				Map securedMap = (Map) httpSession
						.get(FwConstants.SECURED_SESSION);
				if (securedMap != null
						&& securedMap.get(FwConstants.LDAP_LOGON_ID) != null) {
					userId = (String) securedMap
							.get(FwConstants.LDAP_LOGON_ID);
				}
			}

			CUSTOM_ADMIN_WORKER_INBOX_Collection custInboxUpdateColl = (CUSTOM_ADMIN_WORKER_INBOX_Collection) pageCollection
					.get(AppConstants.CUSTOM_ADMIN_WORKER_INBOX_Collection);
			CUSTOM_ADMIN_WORKER_INBOX_Collection custInboxBeforeColl = (CUSTOM_ADMIN_WORKER_INBOX_Collection) beforeCollection
					.get(AppConstants.CUSTOM_ADMIN_WORKER_INBOX_Results_Collection);
			CUSTOM_ADMIN_WORKER_INBOX_Collection custInboxCriteriaColl = (CUSTOM_ADMIN_WORKER_INBOX_Collection) beforeCollection
					.get(AppConstants.CUSTOM_ADMIN_WORKER_INBOX_Collection);

			CUSTOM_ADMIN_WORKER_INBOX_Cargo custInboxCargo = custInboxCriteriaColl
					.getResult(0);

			custInboxCargo.setSsn_num(concatenateSsnNumber(request,
					CafeConstants.SOCIAL_SECURITY));

			int updatedSize = custInboxUpdateColl.size();
			int beforeSize = custInboxBeforeColl.size();
			CUSTOM_ADMIN_WORKER_INBOX_Cargo custInboxUpdateCargo = null;
			CUSTOM_ADMIN_WORKER_INBOX_Cargo custInboxBeforeCargo = null;
			RMC_RQST_HIST_Collection rmcRqstColl = new RMC_RQST_HIST_Collection();
			RMC_RQST_HIST_Cargo rmcRqstCargo = null;

			for (int i = 0; i < updatedSize; i++) {
				custInboxUpdateCargo = custInboxUpdateColl.getResult(i);
				for (int j = 0; j < beforeSize; j++) {
					custInboxBeforeCargo = custInboxBeforeColl.getResult(j);
					if (custInboxBeforeCargo.getCr_num().equals(
							custInboxUpdateCargo.getCr_num())
							&& !custInboxBeforeCargo.getCr_status().equals(
									custInboxUpdateCargo.getCr_status())) {
						rmcRqstCargo = new RMC_RQST_HIST_Cargo();
						rmcRqstCargo.setApp_num(custInboxUpdateCargo
								.getCr_num());
						rmcRqstCargo.setSrc_ind(AppConstants.RMC_RQST_HIST_CC);
						rmcRqstCargo.setApp_stat_cd(custInboxUpdateCargo
								.getCr_status());
						rmcRqstCargo.setUpdt_by(userId);
						rmcRqstCargo.setUpdt_last_tms(fwDate
								.getFormattedDateTime());
						rmcRqstCargo.setRowAction(FwConstants.ROWACTION_INSERT);
						rmcRqstColl.addCargo(rmcRqstCargo);
					}
				}
			}

			pageCollection.put("AfterUpdate", AppConstants.YES);
			pageCollection.put(
					AppConstants.CUSTOM_ADMIN_WORKER_INBOX_Collection,
					custInboxCriteriaColl);

			ADCIBValidator adcibValidator = new ADCIBValidator();
			adcibValidator.validateWorkerInboxUpdate(rmcRqstColl);
			if (adcibValidator.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, adcibValidator
						.getMessageList());
				pageCollection.put(AppConstants.ADCCWorkerInboxResultsListView,
						beforeCollection.get(AppConstants.ADCCWorkerInboxResultsListView));
				return;
			}

			AdminSearchBO adminBO = new AdminSearchBO();
			adminBO.storeWorkInboxStatus(rmcRqstColl);

			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"storeCCWorkInboxStatus",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("storeCCWorkInboxStatus");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"storeCCWorkInboxStatus", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::storeCCWorkInboxStatus::End");
	}
	
	
	
	
	
	
	/**
	 * This method is used to retrieve all the maxstar clients that match the given
	 * criteria.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void getMaxstarSearchResults(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::getMaxstarSearchResults::Start");

		try {

			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			String langInd = FwConstants.ENGLISH; // Language Indicator
			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				langInd = (String) httpSession.get(AppConstants.LANGUAGE);
			}

			CUSTOM_ADMIN_SEARCH_Collection clientRqstAfterColl = (CUSTOM_ADMIN_SEARCH_Collection) pageCollection
					.get(AppConstants.CUSTOM_ADMIN_SEARCH_Collection); // Custom Admin
			// Search Collection
			CUSTOM_ADMIN_SEARCH_Cargo customAdminSearchCargo = clientRqstAfterColl
					.getResult(0); // Custom Admin Search Cargo
			ADMASValidator validator = new ADMASValidator();
			validator.validateMaxstarSearchDetails(customAdminSearchCargo);
			if (validator.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, validator
						.getMessageList());
				pageCollection.put("CUSTOM_ADMIN_SEARCH",
						customAdminSearchCargo);
				return;
			}

			AdminSearchBO adminSearchBO = new AdminSearchBO();
			CUSTOM_ADMIN_SEARCH_Collection searchResults = adminSearchBO
					.loadMaxstarSearchResults(customAdminSearchCargo);
			if (searchResults == null) {
				validator
						.validateMaxstarSearchDetails(customAdminSearchCargo);
				if (validator.hasMessages()) {
					request.put(FwConstants.MESSAGE_LIST, validator
							.getMessageList());
					return;
				}
			}
			
			if (searchResults != null && searchResults.size() > 100) {
				pageCollection.put("MORE_RECORDS", CafeConstants.TRUE);
			}
			String tabId = (String)request.get(FwConstants.TAB_ID);
			ADMaxstarSearchResultsListview listviewbean = new ADMaxstarSearchResultsListview();
			listviewbean.setLanguage(langInd);
			listviewbean.setTabId(tabId);
			// call the set DisplayData
			listviewbean.setDisplayData(searchResults);
			// uniquely name the listview
			listviewbean.setName(AppConstants.ADMaxstarSearchResultsListview_Listview);
			// this oen for the paging
			listviewbean.setPageSize(20);
			// define the columns to be sorted on takes a boolean array
			boolean sortableCols[] = { false, false, false, false, false };
			listviewbean.setSortableColumns(sortableCols);
			// specify the type of the columns in listview
			int[] dataType = { IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol };
			listviewbean.setColDataType(dataType);
			pageCollection
					.put(AppConstants.ADMaxstarSearchResultsListview_Listview, listviewbean);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"getMaxstarSearchResults",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("getMaxstarSearchResults");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"getMaxstarSearchResults", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::getMaxstarSearchResults::End");
	}
	
	
	
	
	/**
	 * This method is used to retrieve all the maxstar clients that match the given
	 * criteria.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadSurveyViewerPage(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::loadSurveyViewerPage::Start");

		try {

			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			
			session.remove(FwConstants.BEFORE_COLLECTION);
			CUSTOM_SURVEY_SEARCH_Cargo surveySearchCargo = new CUSTOM_SURVEY_SEARCH_Cargo();
			CUSTOM_SURVEY_SEARCH_Cargo surveySearchMinsCargo = new CUSTOM_SURVEY_SEARCH_Cargo();
			AdminSurveyViewerBO surveyBo = new AdminSurveyViewerBO();
			
			String pageAction = (String)request.get(FwConstants.PAGE_ACTION);
			if(pageAction!=null && pageAction.equals("ADSVRAFBLoad")){
				pageCollection.put(AppConstants.SURVEY_TYPE,AppConstants.AFB_SURVEY);
				surveySearchCargo.setSurvey_type(AppConstants.AFB_SURVEY);
				surveySearchMinsCargo = surveyBo.getSurveyMinTotals(surveySearchCargo); 
			}else if(pageAction!=null && pageAction.equals("ADSVRRMCLoad")){
				pageCollection.put(AppConstants.SURVEY_TYPE,AppConstants.RMC_SURVEY);
				surveySearchCargo.setSurvey_type(AppConstants.RMC_SURVEY);
				surveySearchMinsCargo = surveyBo.getSurveyMinTotals(surveySearchCargo);
			}else{
				pageCollection.put(AppConstants.SURVEY_TYPE,AppConstants.CMB_SURVEY);
				surveySearchCargo.setSurvey_type(AppConstants.CMB_SURVEY);
			}
			
			surveySearchCargo = surveyBo.getSurveyTotals(surveySearchCargo);
			pageCollection.put(AppConstants.CUSTOM_SURVEY_SEARCH_MINS_Cargo, surveySearchMinsCargo);
			pageCollection.put(AppConstants.CUSTOM_SURVEY_SEARCH_Cargo, surveySearchCargo);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"loadSurveyViewerPage",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("loadSurveyViewerPage");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"loadSurveyViewerPage", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::loadSurveyViewerPage::End");
	}	
	
	
	/**
	 * This method is used to retrieve all the survey viewer data that match the given
	 * criteria.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadSurveyViewerData(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::loadSurveyViewerData::Start");

		try {

			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			String pageAction = (String)request.get(FwConstants.PAGE_ACTION);

			Map beforeMap = null;
			
			if(session.get(FwConstants.BEFORE_COLLECTION)!=null){
				beforeMap = (Map)session.get(FwConstants.BEFORE_COLLECTION);
			}
			
			CUSTOM_SURVEY_SEARCH_Collection searchCollection = (CUSTOM_SURVEY_SEARCH_Collection) pageCollection
														.get(AppConstants.CUSTOM_SURVEY_SEARCH_Collection); // 
			
			CUSTOM_SURVEY_SEARCH_Cargo searchCargo = (CUSTOM_SURVEY_SEARCH_Cargo)searchCollection.get(0);
			
			CUSTOM_SURVEY_SEARCH_Cargo surveySearchCargo = null;
			CUSTOM_SURVEY_SEARCH_Cargo surveySearchData = new CUSTOM_SURVEY_SEARCH_Cargo();
			AdminSurveyViewerBO surveyBo = new AdminSurveyViewerBO();
			
			ADSVRValidator  adsvrValidator = new ADSVRValidator();
			if(beforeMap!=null && beforeMap.get(AppConstants.SURVEY_TYPE)!=null){
				pageCollection.put(AppConstants.SURVEY_TYPE,beforeMap.get(AppConstants.SURVEY_TYPE));
				searchCargo.setSurvey_type((String)beforeMap.get(AppConstants.SURVEY_TYPE));
			}
			if(pageAction!=null && pageAction.equals("ADSVRSearch")){
				adsvrValidator.validateCMBSearchCriteria(searchCargo);
				if(adsvrValidator.hasMessages()){
					request.put(FwConstants.MESSAGE_LIST, adsvrValidator
							.getMessageList());
					pageCollection.put(AppConstants.SURVEY_SEARCH_CARGO, searchCargo);
					if(beforeMap!=null){
						pageCollection.put(AppConstants.CUSTOM_SURVEY_SEARCH_Cargo, 
								beforeMap.get(AppConstants.CUSTOM_SURVEY_SEARCH_Cargo));
						pageCollection.put(AppConstants.CUSTOM_SURVEY_SEARCH_MINS_Cargo, 
								beforeMap.get(AppConstants.CUSTOM_SURVEY_SEARCH_MINS_Cargo));						
					}	
					return;
				}
				searchCargo.setRow_number(null);
				pageCollection.put(AppConstants.SURVEY_SEARCH_CARGO, searchCargo);
				surveySearchCargo = surveyBo.getTotalSurveySearchCount(searchCollection);
				pageCollection.put(AppConstants.SURVEY_SEARCH_COUNT, surveySearchCargo.getCount());
				if(surveySearchCargo.getCount()!=null 
						&& !surveySearchCargo.getCount().equals(AppConstants.ZERO)){
					surveySearchData =surveyBo.getSurveySearchResults(searchCollection) ;
				}
				
				if(beforeMap!=null){
					pageCollection.put(AppConstants.CUSTOM_SURVEY_SEARCH_Cargo, 
							beforeMap.get(AppConstants.CUSTOM_SURVEY_SEARCH_Cargo));
					pageCollection.put(AppConstants.CUSTOM_SURVEY_SEARCH_MINS_Cargo, 
							beforeMap.get(AppConstants.CUSTOM_SURVEY_SEARCH_MINS_Cargo));					
				}	

				pageCollection.put(AppConstants.SURVEY_SEARCH_DATA,surveySearchData);
				
			}else{
				
				CUSTOM_SURVEY_SEARCH_Cargo  beforeSearchCargo = null;
				CUSTOM_SURVEY_SEARCH_Collection  beforeSearchCollection = new CUSTOM_SURVEY_SEARCH_Collection();
				if(beforeMap!=null){
					beforeSearchCargo = (CUSTOM_SURVEY_SEARCH_Cargo)beforeMap.get(AppConstants.SURVEY_SEARCH_CARGO);
				}else{
					beforeSearchCargo = new CUSTOM_SURVEY_SEARCH_Cargo();
				}
				
				
				if(pageAction!=null && pageAction.equals("ADSVRFirst")){
					beforeSearchCargo.setRow_number(AppConstants.ONE);

				}else if(pageAction!=null && pageAction.equals("ADSVRNext")){
					int row = 1;
					if(searchCargo.getRow_number()!=null){
						row = Integer.parseInt(searchCargo.getRow_number());
						row = row+1;
					}
					beforeSearchCargo.setRow_number(String.valueOf(row));

				}else if(pageAction!=null && pageAction.equals("ADSVRPrevious")){
					int row = 1;
					if(searchCargo.getRow_number()!=null){
						row = Integer.parseInt(searchCargo.getRow_number());
						if(row>1){
							row = row-1;
						}
					}

					beforeSearchCargo.setRow_number(String.valueOf(row));
				}else if(pageAction!=null && pageAction.equals("ADSVRLast")){
					String last = (String)request.get("survey_end");

					beforeSearchCargo.setRow_number(last);
				}else if(pageAction!=null && pageAction.equals("ADSVRFind")){
					
					adsvrValidator.validateCMBFind(searchCargo.getRow_number(),
							(String)request.get("totalSurveys"));
					if(adsvrValidator.hasMessages()){
						request.put(FwConstants.MESSAGE_LIST, adsvrValidator
								.getMessageList());						
						if(beforeMap!=null){
							pageCollection.put(AppConstants.CUSTOM_SURVEY_SEARCH_Cargo, 
									beforeMap.get(AppConstants.CUSTOM_SURVEY_SEARCH_Cargo));
							pageCollection.put(AppConstants.CUSTOM_SURVEY_SEARCH_MINS_Cargo, 
									beforeMap.get(AppConstants.CUSTOM_SURVEY_SEARCH_MINS_Cargo));							
							pageCollection.put(AppConstants.SURVEY_SEARCH_COUNT, beforeMap.get(AppConstants.SURVEY_SEARCH_COUNT));
							pageCollection.put(AppConstants.SURVEY_SEARCH_DATA, beforeMap.get(AppConstants.SURVEY_SEARCH_DATA));
						}
						pageCollection.put(AppConstants.SURVEY_SEARCH_CARGO, beforeSearchCargo);
						return;
					}
					beforeSearchCargo.setRow_number(searchCargo.getRow_number());
				}
				pageCollection.put(AppConstants.SURVEY_SEARCH_CARGO, beforeSearchCargo);
				
				beforeSearchCollection.add(beforeSearchCargo);
				
				surveySearchData =surveyBo.getSurveySearchResults(beforeSearchCollection) ;
				pageCollection.put(AppConstants.SURVEY_SEARCH_DATA,surveySearchData);
				
				if(beforeMap!=null){
					pageCollection.put(AppConstants.CUSTOM_SURVEY_SEARCH_Cargo, 
							beforeMap.get(AppConstants.CUSTOM_SURVEY_SEARCH_Cargo));
					pageCollection.put(AppConstants.CUSTOM_SURVEY_SEARCH_MINS_Cargo, 
							beforeMap.get(AppConstants.CUSTOM_SURVEY_SEARCH_MINS_Cargo));					
					pageCollection.put(AppConstants.SURVEY_SEARCH_COUNT, beforeMap.get(AppConstants.SURVEY_SEARCH_COUNT));
				}
			}
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"loadSurveyViewerData",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("loadSurveyViewerData");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"loadSurveyViewerData", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::loadSurveyViewerData::End");
	}	
	
	
	/**
	 * This method loads the reference table data for a given reference table
	 * name.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadProviderAdminData(FwTransaction txnBean)
			throws javax.ejb.EJBException {

		log(ILog.INFO, "AdminEJBBean::loadProviderAdminData::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			String langInd = FwConstants.ENGLISH; // Language Indicator
			String tableId = null; // Table Id

			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				langInd = (String) httpSession.get(AppConstants.LANGUAGE);
			}

			// Removing Previous session Values

			session.remove(CafeConstants.REFERENCE_TABLE_DATA_COLLECTION);

			CUSTOM_REFERENCE_TABLE_DATA_Collection customRefDataColl = (CUSTOM_REFERENCE_TABLE_DATA_Collection) pageCollection
					.get(AppConstants.CUSTOM_REFERENCE_TABLE_DATA_Collection);
			if (customRefDataColl != null) {
				CUSTOM_REFERENCE_TABLE_DATA_Cargo customRefDataCargo = customRefDataColl
						.getResult(0);

				tableId = customRefDataCargo.getLkup_grp_cd();
			} else {
				tableId = (String) request.get("referenceValues_tablename");
			}

			String refTableId = null; // Reference Table Id
			String refTabDesc = null; // Reference Table Description

			AdminSearchBO adminSearchBO = new AdminSearchBO();

			CUSTOM_REFERENCE_TABLE_DATA_Collection refDataCollection = adminSearchBO
					.loadReferenceData(tableId); // Cutom Reference Table
			// Data Collection
			CUSTOM_REFERENCE_TABLE_DATA_Collection filterCollection = new CUSTOM_REFERENCE_TABLE_DATA_Collection(); // Cutom
			// Reference
			// Table
			// Data
			// Collection
			CUSTOM_REFERENCE_TABLE_DATA_Cargo filterCargo = null; // Cutom
			// Reference
			// Table
			// Data
			// Cargo
			CUSTOM_REFERENCE_TABLE_DATA_Cargo tmpCargo = null; // Cutom
			// Reference
			// Table Data
			// Cargo
			if (refDataCollection != null && refDataCollection.size() > 0) {

				tmpCargo = (CUSTOM_REFERENCE_TABLE_DATA_Cargo) refDataCollection
						.get(0);
				String lkupFldId = tmpCargo.getLkup_fld_id(); // Lookup field
				// id
				refTableId = tmpCargo.getLkup_grp_cd();
				refTabDesc = tmpCargo.getLkup_grp_dsc();

				int size = refDataCollection.size();
	
				for (int i = 0; i < size; i++) {
					filterCargo = (CUSTOM_REFERENCE_TABLE_DATA_Cargo) refDataCollection
							.get(i);
					if (filterCargo != null
							&& filterCargo.getLkup_fld_id().equalsIgnoreCase(
									lkupFldId)) {
						filterCollection.add(filterCargo);
					}

				}

			} else {
				LKUP_GRP_Collection searchResults = adminSearchBO
						.loadReferenceTables(tableId);
				if (searchResults != null && searchResults.size() > 0) {
					LKUP_GRP_Cargo lkupCargo = (LKUP_GRP_Cargo) searchResults
							.get(0);
					refTableId = lkupCargo.getLkup_grp_cd();
					refTabDesc = lkupCargo.getLkup_grp_dsc();

				}
			}

			session.put(CafeConstants.REFERENCE_TABLE_DATA_COLLECTION, refDataCollection);

			ADReferenceDataListView listviewbean = new ADReferenceDataListView();

			listviewbean.setLanguage(langInd);
			// call the set DisplayData
			listviewbean.setDisplayData(filterCollection);
			// uniquely name the listview
			listviewbean.setName(AppConstants.ADReferenceDataListView_Listview);
			// this oen for the paging
			listviewbean.setPageSize(20);
			// define the columns to be sorted on takes a boolean array
			boolean sortableCols[] = { false, false, false, false, false };
			listviewbean.setSortableColumns(sortableCols);
			// specify the type of the columns in listview
			int[] dataType = { IListviewFormatter.stringCol,
					IListviewFormatter.stringCol };
			listviewbean.setColDataType(dataType);
			pageCollection
					.put(AppConstants.ADReferenceDataListView_Listview, listviewbean);
			pageCollection.put(CafeConstants.REF_TAB_ID, refTableId);
			pageCollection.put(CafeConstants.REF_TABLE_DESC, refTabDesc);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),CafeConstants.LOAD_REFERENCE_DATA,fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID(CafeConstants.LOAD_REFERENCE_DATA);
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					CafeConstants.LOAD_REFERENCE_DATA, e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::loadReferenceData::End");

	}	
	
	
	/**
	 * This method is used to load all the notification text (from CP_NOTIFICATIONS)
	 * matching the given criteria (text id or text desc).
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadNotificationSearchResults(FwTransaction txnBean)
			throws javax.ejb.EJBException {

		log(ILog.INFO, "AdminEJBBean::loadNotificationSearchResults::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			String langInd = FwConstants.ENGLISH; // Language Indicator

			AdminSearchBO adminSearchBO = new AdminSearchBO();

			CP_NOTIFICATIONS_Collection searchResults = adminSearchBO
					.displayNotificationsData();
			ADAnnouncementsSearchDplyListView listviewbean = new ADAnnouncementsSearchDplyListView();
			listviewbean.setLanguage(langInd);
			// call the set DisplayData
			listviewbean.setDisplayData(searchResults);
			// uniquely name the listview
			listviewbean.setName(AppConstants.ADAnnouncementsSearchDplyListView_Listview);
			// this oen for the paging
			listviewbean.setPageSize(20);
			// define the columns to be sorted on takes a boolean array
			boolean sortableCols[] = { false, false, false,false };
			listviewbean.setSortableColumns(sortableCols);
			// specify the type of the columns in listview
			int[] dataType = { IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol , IListviewFormatter.stringCol };
			listviewbean.setColDataType(dataType);
			pageCollection.put(AppConstants.ADAnnouncementsSearchDplyListView_Listview, listviewbean);
			pageCollection.put(CafeConstants.CP_NOTIFICATIONS_COLLECTION, searchResults);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"loadNotificationSearchResults",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("loadNotificationSearchResults");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"loadNotificationSearchResults", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::loadNotificationSearchResults::End");
	}	
	
	
	
	/**
	 * This method is used to load the text details for a given text id for
	 * editing.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadAnnouncementsData(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::loadAnnouncementsData::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			
			CP_NOTIFICATIONS_Cargo announcementsCargo = null; // dply text cargo
			CP_NOTIFICATIONS_Cargo cargo = null; // dply text cargo
			CP_NOTIFICATIONS_Collection searchResult = null; // dply text collection
			CP_NOTIFICATIONS_Collection announcementsCollection = null; // dply text
			// collection
			CP_NOTIFICATIONS_Collection dplyCollection = (CP_NOTIFICATIONS_Collection) pageCollection
					.get(AppConstants.CP_NOTIFICATIONS_Collection); // dply text collection

			if (session.get(FwConstants.BEFORE_COLLECTION) != null) {
				Map pCollection = (Map) session
						.get(FwConstants.BEFORE_COLLECTION);

				if (pCollection.get(CafeConstants.CP_NOTIFICATIONS_COLLECTION) != null) {

					searchResult = (CP_NOTIFICATIONS_Collection) pCollection
							.get(CafeConstants.CP_NOTIFICATIONS_COLLECTION);

				}
			}

			if (dplyCollection != null && dplyCollection.size() > 0) {
				announcementsCargo = (CP_NOTIFICATIONS_Cargo) dplyCollection.getCargo(0);

				announcementsCollection = new CP_NOTIFICATIONS_Collection();

				for (int i = 0; searchResult != null && i < searchResult.size(); i++) {

					cargo = (CP_NOTIFICATIONS_Cargo) searchResult.get(i);
					if (announcementsCargo.getNotice_id() != null
							&& !(announcementsCargo.getNotice_id().equals(""))
							&& announcementsCargo.getNotice_id().equals(cargo.getNotice_id())) {						
						announcementsCollection.add(cargo);
					}

				}

			}

			pageCollection.put(CafeConstants.ANNOUNCEMENTS_COLLECTION, announcementsCollection);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"loadAnnouncementsData",fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("loadAnnouncementsData");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"loadAnnouncementsData", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::loadAnnouncementsData::End");
	}	
	
	
	
	/**
	 * This method stores the edited/added display texts.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void storeAnnouncementsData(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::storeAnnouncementsData::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			String langInd = FwConstants.ENGLISH; // Language Indicator
			String userId = null;
			Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
			if (httpSession != null) {
				langInd = (String) httpSession.get(AppConstants.LANGUAGE);
				Map securedSessMap = (Map) httpSession
				.get(FwConstants.SECURED_SESSION);
				if (securedSessMap != null) {
					userId = (String) securedSessMap
							.get(FwConstants.LDAP_LOGON_ID);
				}
			}
			
			String noticeId = (String) request.get("label_2");// dply
			// text
			// id
			String action = (String) request.get(CafeConstants.ACTION); // action
			// description
			String noticeTxt = (String) request.get("label_1"); // comment
			// text
			String startDate = (String) request.get("startDate"); // module code
			String endDate = (String) request.get("endDate"); // module code	
			String noticeTxtId = null; // dply text id

			CP_NOTIFICATIONS_Collection dplyTxtCollection = new CP_NOTIFICATIONS_Collection();// dply
			// text
			// collection
			CP_NOTIFICATIONS_Collection dplyCollection = null; // dply text collection
			CP_NOTIFICATIONS_Cargo enCargo = new CP_NOTIFICATIONS_Cargo(); // dply text cargo
			CP_NOTIFICATIONS_Cargo cargo = null; // dply text cargo
			AdminSearchBO adminBO = new AdminSearchBO();
			enCargo.setRowAction(FwConstants.ROWACTION_INSERT);
			enCargo.setNotice_id(noticeId);
			enCargo.setNotice_txt(noticeTxt);
			enCargo.setNotice_strt_dt(startDate);
			enCargo.setNotice_end_dt(endDate);
			enCargo.setUser_id(userId);
			
			ADCEDValidator adedtValidator = new ADCEDValidator();
			adedtValidator.validateAnnouncementData(noticeTxt, startDate, endDate);
			if (adedtValidator.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, adedtValidator
						.getMessageList());
				dplyTxtCollection.add(enCargo);
				pageCollection.put(CafeConstants.ANNOUNCEMENTS_COLLECTION, dplyTxtCollection);
				return;
			}			

			enCargo.setNotice_strt_dt(displayFormatter.getYYYYMMDDDate(startDate));
			enCargo.setNotice_end_dt(displayFormatter.getYYYYMMDDDate(endDate));


			if (action != null && action.equalsIgnoreCase(CafeConstants.UPDATE)) {

				if (session.get(FwConstants.BEFORE_COLLECTION) != null) {
					Map pCollection = (Map) session
							.get(FwConstants.BEFORE_COLLECTION);

					if (pCollection.get(CafeConstants.ANNOUNCEMENTS_COLLECTION) != null) {

						dplyCollection = (CP_NOTIFICATIONS_Collection) pCollection
								.get(CafeConstants.ANNOUNCEMENTS_COLLECTION);

					}
				}

				if (dplyCollection != null && dplyCollection.size() > 0) {

					int size = dplyCollection.size();
					for (int i = 0; i < size; i++) {
						cargo = (CP_NOTIFICATIONS_Cargo) dplyCollection.get(i);

						noticeTxtId = cargo.getNotice_id();

						if (noticeTxtId != null && !noticeTxtId.equalsIgnoreCase("")
								&& noticeTxtId.equalsIgnoreCase(noticeId)) {
							enCargo.setRowAction(FwConstants.ROWACTION_UPDATE);
							enCargo.setNotice_id(noticeTxtId);
						}


					}
				}
			} else {
				enCargo.setNotice_id(noticeId);

			}
			dplyTxtCollection.add(enCargo);

			adminBO.storeAnnouncementsTxtData(dplyTxtCollection);
			
			
			// If data persist is successful then displaylatest data in search
			// page.
			
			
			ADAnnouncementsSearchDplyListView listviewbean = new ADAnnouncementsSearchDplyListView();
			listviewbean.setLanguage(langInd);
			// call the set DisplayData
			listviewbean.setDisplayData(dplyTxtCollection);
			// uniquely name the listview
			listviewbean.setName(AppConstants.ADAnnouncementsSearchDplyListView_Listview);
			// this oen for the paging
			listviewbean.setPageSize(20);
			// define the columns to be sorted on takes a boolean array
			boolean sortableCols[] = { false, false, false,false };
			listviewbean.setSortableColumns(sortableCols);
			// specify the type of the columns in listview
			int[] dataType = { IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol , IListviewFormatter.stringCol };
			listviewbean.setColDataType(dataType);
			pageCollection.put(AppConstants.ADAnnouncementsSearchDplyListView_Listview, listviewbean);
			pageCollection.put(CafeConstants.CP_NOTIFICATIONS_COLLECTION, dplyTxtCollection);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);


		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),CafeConstants.STORE_ANNOUNCEMENTS_DATA,fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID(CafeConstants.STORE_ANNOUNCEMENTS_DATA);
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					CafeConstants.STORE_ANNOUNCEMENTS_DATA, e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::storeAnnouncementsData::End");
	}	
	
	
	/**
	 * This method stores the edited/added display texts.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void deleteAnnouncementsData(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "AdminEJBBean::deleteAnnouncementsData::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map pageCollection = txnBean.getPageCollection(); // Page
						AdminSearchBO adminBO = new AdminSearchBO();
            String noticeId = null;
            CP_NOTIFICATIONS_Collection dplyTxtCollection = new CP_NOTIFICATIONS_Collection();
			CP_NOTIFICATIONS_Cargo announcementsCargo = null; // dply text cargo
			CP_NOTIFICATIONS_Cargo cargo = null; // dply text cargo
			CP_NOTIFICATIONS_Cargo enCargo = new CP_NOTIFICATIONS_Cargo(); // dply text cargo
			CP_NOTIFICATIONS_Collection searchResult = null; // dply text collection
			CP_NOTIFICATIONS_Collection announcementsCollection = null; // dply text
			// collection
			CP_NOTIFICATIONS_Collection dplyCollection = (CP_NOTIFICATIONS_Collection) pageCollection
					.get(AppConstants.CP_NOTIFICATIONS_Collection); // dply text collection

			if (session.get(FwConstants.BEFORE_COLLECTION) != null) {
				Map pCollection = (Map) session
						.get(FwConstants.BEFORE_COLLECTION);

				if (pCollection.get(CafeConstants.CP_NOTIFICATIONS_COLLECTION) != null) {

					searchResult = (CP_NOTIFICATIONS_Collection) pCollection
							.get(CafeConstants.CP_NOTIFICATIONS_COLLECTION);

				}
			}
			
			if (dplyCollection != null && dplyCollection.size() > 0) {
				announcementsCargo = (CP_NOTIFICATIONS_Cargo) dplyCollection.getCargo(0);
				announcementsCollection = new CP_NOTIFICATIONS_Collection();
				
				for (int i = 0; searchResult != null && i < searchResult.size(); i++) {

					cargo = (CP_NOTIFICATIONS_Cargo) searchResult.get(i);
					if (announcementsCargo.getNotice_id() != null
							&& !(announcementsCargo.getNotice_id().equals("")) && announcementsCargo.getNotice_id().equals(cargo.getNotice_id())) {		
						noticeId = announcementsCargo.getNotice_id();
						announcementsCollection.add(cargo);
					}

				}

			}			

			enCargo.setRowAction(FwConstants.ROWACTION_DELETE);
			enCargo.setNotice_id(noticeId);


			dplyTxtCollection.add(enCargo);

			adminBO.storeAnnouncementsTxtData(dplyTxtCollection);
			


		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),CafeConstants.STORE_ANNOUNCEMENTS_DATA,fe);
			we.setStackTrace(fe.getStackTrace());
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID(CafeConstants.STORE_ANNOUNCEMENTS_DATA);
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"deleteAnnouncementsData", e);
			throw fe;
		}
		log(ILog.INFO, "AdminEJBBean::deleteAnnouncementsData::End");
	}	
	
}
