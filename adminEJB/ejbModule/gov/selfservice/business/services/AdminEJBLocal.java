package gov.selfservice.business.services;

import javax.ejb.Local;

import gov.selfservice.framework.business.entities.FwTransaction;

/**
 * Local interface for Enterprise Bean:RegistrationEJB
 */
@Local

public interface AdminEJBLocal{

	public void getEFormSearchResults(FwTransaction txnBean)
			;

	public void getRefTableData(FwTransaction txnBean)
			;

	public void getRefTableDomains(FwTransaction txnBean)
			;

	public void loadRefTableDomain(FwTransaction txnBean)
			;

	public void sotreRefTableDomains(FwTransaction txnBean)
			;

	public void loadReferenceData(FwTransaction txnBean)
			;

	public void storeReferenceData(FwTransaction txnBean)
			;

	public void loadReferenceRowData(FwTransaction txnBean)
			;

	public void storeReferenceRowData(FwTransaction txnBean)
			;

	public void loadReferenceAddData(FwTransaction txnBean)
			;

	public void loadRTAdd(FwTransaction txnBean) ;

	public void storeRTTable(FwTransaction txnBean)
			;

	public void loadMSGData(FwTransaction txnBean)
			;

	public void displayMSGData(FwTransaction txnBean)
			;

	public void storeMSGData(FwTransaction txnBean)
			;

	public void displayDplyTxtData(FwTransaction txnBean)
			;

	public void loadDplyTxtData(FwTransaction txnBean)
			;

	public void storeDplyTxtData(FwTransaction txnBean)
			;

	public void searchUser(FwTransaction txnBean) ;

	public void storeSecurityRoles(FwTransaction txnBean)
			;

	public void storeSecurityBusinessFunctions(FwTransaction txnBean)
			;

	public void loadBusinessFunctions(FwTransaction txnBean)
			;

	public void searchPagesForBusinessFunctions(FwTransaction txnBean)
			;

	public void storeMaintainBusinessFunctions(FwTransaction txnBean)
			;

	public void loadSecurityRoles(FwTransaction txnBean)
			;

	public void searchBusinessFunctionsForRole(FwTransaction txnBean)
			;

	public void storeMaintainRoles(FwTransaction txnBean)
			;

	public void loadAssignRoles(FwTransaction txnBean)
			;

	public void storeAssignRoles(FwTransaction txnBean)
			;

	public void loadBatchData(FwTransaction txnBean)
			;

	public void storeBatchData(FwTransaction txnBean)
			;
	
	public void serverMonitor(FwTransaction txnBean)
			;
	
	public void loadWorkInboxResults(FwTransaction txnBean)
			;
	
	public void storeWorkInboxStatus(FwTransaction txnBean) 
			;
	
	public void loadCCWorkInboxResults(FwTransaction txnBean)
			;

	public void storeCCWorkInboxStatus(FwTransaction txnBean) 
			;
	
	public void getMaxstarSearchResults(FwTransaction txnBean)
			;
	
	public void loadSurveyViewerData(FwTransaction txnBean)
		;
	
	public void loadSurveyViewerPage(FwTransaction txnBean)
		;
	
	public void loadNotificationSearchResults(FwTransaction txnBean)
		;	
	
	public void loadAnnouncementsData(FwTransaction txnBean)
		;	
	
	public void storeAnnouncementsData(FwTransaction txnBean)
		;	
	
	public void deleteAnnouncementsData(FwTransaction txnBean)
		;		
	
}