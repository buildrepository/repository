package gov.selfservice.business.services;

import javax.ejb.Local;

import gov.selfservice.framework.business.entities.FwTransaction;


/**
 * Local interface for Enterprise Bean:PageElementAdminEJB
 */
@Local
public interface PageElementAdminEJBLocal{
	public void loadElementSearchResults(FwTransaction txnBean)
	;
	
	public void loadElementDetails(FwTransaction txnBean)
	;
	
	public void storeElementDetails(FwTransaction txnBean)
	;
	
	public void searchPage(FwTransaction txnBean)
	;
	
	public void storePageDetails(FwTransaction txnBean)
	;

	public void loadPageDetails(FwTransaction txnBean)
	;	
	
	public void loadPageElementList(FwTransaction txnBean)
	;
	
	public void loadPageElementDetails(FwTransaction txnBean)
	;
	
	public void storePageElementDetails(FwTransaction txnBean)
	;
}