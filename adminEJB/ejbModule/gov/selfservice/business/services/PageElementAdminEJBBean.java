package gov.selfservice.business.services;

import gov.selfservice.business.customEntities.CUSTOM_PAGE_ELE_DATA_Cargo;
import gov.selfservice.business.customEntities.CUSTOM_PAGE_ELE_DATA_Collection;
import gov.selfservice.business.entities.ELE_DPLY_Cargo;
import gov.selfservice.business.entities.ELE_DPLY_Collection;
import gov.selfservice.business.entities.PAGE_Cargo;
import gov.selfservice.business.entities.PAGE_Collection;
import gov.selfservice.business.entities.PAGE_ELE_Cargo;
import gov.selfservice.business.entities.PAGE_ELE_Collection;
import gov.selfservice.business.rules.admin.ADElementBO;
import gov.selfservice.business.rules.admin.AdminPageBO;
import gov.selfservice.business.validation.admin.search.ADAEDValidator;
import gov.selfservice.business.validation.admin.search.ADAEPValidator;
import gov.selfservice.business.validation.admin.search.ADSFPValidator;
import gov.selfservice.business.validation.admin.search.ADUPDValidator;
import gov.selfservice.framework.business.entities.FwTransaction;
import gov.selfservice.framework.exceptions.FwException;
import gov.selfservice.framework.exceptions.FwWrappedException;
import gov.selfservice.framework.management.constants.FwConstants;
import gov.selfservice.framework.management.logging.ILog;
import gov.selfservice.framework.presentation.entities.listview.IListviewFormatter;
import gov.selfservice.management.constants.AppConstants;
import gov.selfservice.presentation.entities.listview.ADElementSearchListView;
import gov.selfservice.presentation.entities.listview.ADPageElementDataListView;
import gov.selfservice.presentation.entities.listview.ADSearchPageListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

/**
 * PageElementAdminEJB - SessionFacade Object
 * skeleton auto generated - Architecture Team
 * @Creation Date :Nov 11 2008
 * Modified By:
 * Modified on:
 */

@Stateless

public class PageElementAdminEJBBean extends AppSessionBean implements PageElementAdminEJBLocal {

	/**
	 * Constructor
	 */
	public PageElementAdminEJBBean() {
	}

	/**
	 * This method is used to load all the display texts (from DPLY_TXT)
	 * matching the given criteria (text id or text desc).
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadElementSearchResults(FwTransaction txnBean)
			throws javax.ejb.EJBException {

		log(ILog.INFO, "PageElementAdminEJBBean::loadElementSearchResults::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection();			
			
			ELE_DPLY_Collection afterEleDplyColl = new ELE_DPLY_Collection();
			ELE_DPLY_Collection eleDplySearchResultsColl = new ELE_DPLY_Collection();
			ELE_DPLY_Cargo afterEleDplyCargo = new ELE_DPLY_Cargo();			
			
			String langInd = FwConstants.ENGLISH; // Language Indicator

			if (session != null) {
				Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
				if (httpSession != null) {
					langInd = (String) httpSession.get(AppConstants.LANGUAGE);
				}
			}
			
			afterEleDplyColl = (ELE_DPLY_Collection)pageCollection.get(AppConstants.ELE_DPLY_Collection);
			
			if (afterEleDplyColl==null && request.get(FwConstants.PREVIOUS_PAGE_ID)!=null && request.get(FwConstants.PREVIOUS_PAGE_ID).equals("ADAED")){
				Map beforeColl = (Map)session.get(FwConstants.BEFORE_COLLECTION);
				if (beforeColl!=null){
					afterEleDplyColl = (ELE_DPLY_Collection)beforeColl.get(AppConstants.ELE_DPLY_Collection);
				}				
			}
			
			
			if (afterEleDplyColl!=null && afterEleDplyColl.size() > 0){
				afterEleDplyCargo = afterEleDplyColl.getCargo();
			}
			

			ADElementBO elementBO = new ADElementBO();
			eleDplySearchResultsColl = elementBO.getElementSearchResults(afterEleDplyCargo);			
			
			ADElementSearchListView listviewbean = new ADElementSearchListView();
			listviewbean.setLanguage(langInd);
			// call the set DisplayData
			listviewbean.setDisplayData(eleDplySearchResultsColl);
			// uniquely name the listview
			listviewbean.setName(AppConstants.ADElementSearchListView_Listview);
			// this one for the paging
			listviewbean.setPageSize(20);
			// define the columns to be sorted on takes a boolean array
			boolean sortableCols[] = { false, false, false, false, false };
			listviewbean.setSortableColumns(sortableCols);
			// specify the type of the columns in listview
			int[] dataType = { IListviewFormatter.stringCol,IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol, IListviewFormatter.stringCol };
			listviewbean.setColDataType(dataType);
			pageCollection.put(AppConstants.ADElementSearchListView_Listview, listviewbean);
			pageCollection.put(AppConstants.ELE_DPLY_Collection, afterEleDplyColl);
			if (session!=null) {
				session.put(FwConstants.BEFORE_COLLECTION, pageCollection);
			}

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"loadElementSearchResults",fe);
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("loadElementSearchResults");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"loadElementSearchResults", e);
			throw fe;
		}
		log(ILog.INFO, "PageElementAdminEJBBean::loadElementSearchResults::End");
	}

	/**
	 * This method is used to load the text details for a given text id for
	 * editing.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadElementDetails(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "PageElementAdminEJBBean::loadElementDetails::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map pageCollection = txnBean.getPageCollection(); // Page

			ELE_DPLY_Cargo eleCargo = null; 		
			ELE_DPLY_Collection eleCollection = new ELE_DPLY_Collection(); 
			
			// collection
			ELE_DPLY_Collection eleDplyCollection = (ELE_DPLY_Collection) pageCollection
					.get(AppConstants.ELE_DPLY_Collection); // dply text collection
			
			String action = AppConstants.NEW;
			
			if (eleDplyCollection != null && eleDplyCollection.size() > 0) {
				eleCargo = (ELE_DPLY_Cargo) eleDplyCollection.getResult(0);
				action = eleCargo.getEle_dsc();
				
				ADElementBO elementBO = new ADElementBO();				
				eleCollection = elementBO.getElementDetails(eleCargo);		

			}
			pageCollection.put(AppConstants.ELE_DPLY_Collection, eleCollection);
			pageCollection.put(AppConstants.ACTION, action);
			
			if (session!=null) {
				session.put(FwConstants.BEFORE_COLLECTION, pageCollection);
			}

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"loadElementDetails",fe);
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("loadElementDetails");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"loadElementDetails", e);
			throw fe;
		}
		log(ILog.INFO, "PageElementAdminEJBBean::loadElementDetails::End");
	}

	/**
	 * This method stores the edited/added display texts.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void storeElementDetails(FwTransaction txnBean)
			throws javax.ejb.EJBException {
		log(ILog.INFO, "PageElementAdminEJBBean::storeElementDetails::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page

			ELE_DPLY_Collection eleDplyCollection = (ELE_DPLY_Collection) pageCollection.get(AppConstants.ELE_DPLY_Collection); 
			ELE_DPLY_Cargo eleDplyCargo = eleDplyCollection.getCargo();
			
			ELE_DPLY_Collection eleDplyBeforeCollection = null;
			ELE_DPLY_Cargo eleDplyBeforeCargo = null;
						
			Map beforeCollection=null;
			if (session!=null) {
				beforeCollection = (Map) session
						.get(FwConstants.BEFORE_COLLECTION);
			}
			if (beforeCollection!=null && beforeCollection.get(AppConstants.ELE_DPLY_Collection) != null) {
				eleDplyBeforeCollection = (ELE_DPLY_Collection) beforeCollection.get(AppConstants.ELE_DPLY_Collection);
			}
			
			if (eleDplyBeforeCollection != null && eleDplyBeforeCollection.size() > 0) {
				eleDplyBeforeCargo = eleDplyBeforeCollection.getCargo();				
			}
			
			String action = (String) request.get(AppConstants.ACTION); // action
						
			ADAEDValidator adaedValidator = new ADAEDValidator();
			adaedValidator.validateADAEDData(eleDplyCargo, eleDplyBeforeCargo);
			if (adaedValidator.hasMessages()) {
				String reqWarningMsgs = (String) request.get(FwConstants.WARNING_MSG_DETAILS);
				
				if (!this.checkForWarningMesgs(reqWarningMsgs,adaedValidator.getMessageList())){
					request.put(FwConstants.MESSAGE_LIST, adaedValidator
							.getMessageList());
					pageCollection.put(AppConstants.ELE_DPLY_Collection, eleDplyCollection);
					pageCollection.put(AppConstants.ACTION, action);
					return;
				}
			}			
			
			if (action != null && action.equalsIgnoreCase(AppConstants.EXISTING)) {
				
				if (eleDplyBeforeCargo != null) {
					eleDplyCargo.setEle_id(eleDplyBeforeCargo.getEle_id());
					eleDplyCargo.setRowAction(FwConstants.ROWACTION_UPDATE);
				}				
			} 
			else {
				eleDplyCargo.setRowAction(FwConstants.ROWACTION_INSERT);
			}		
			
			ADElementBO elementBO = new ADElementBO();
			elementBO.storeElementDetails(eleDplyCollection);


			pageCollection.put(AppConstants.ELE_DPLY_Collection, eleDplyCollection);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"storeElementDetails",fe);
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("storeElementDetails");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"storeElementDetails", e);
			throw fe;
		}
		log(ILog.INFO, "PageElementAdminEJBBean::storeElementDetails::End");
	}
	
	
	
	
	
	
	
	/**
	 * This method retrieves and displays all the Pages that match the given
	 * criteria (pageid).
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void searchPage(FwTransaction txnBean) throws javax.ejb.EJBException {
		log(ILog.INFO, "PageElementAdminEJBBean::searchPage::Start");

		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			String langInd = FwConstants.ENGLISH; // Language Indicator
			if (session != null) {
				Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
				if (httpSession != null) {
					langInd = (String) httpSession.get(AppConstants.LANGUAGE);
				}
			}

			String userId = (String) request.get("pageId"); // page id

			PAGE_Collection admPageCollection = new PAGE_Collection(); // Adm
			// User
			// Collection
			PAGE_Cargo pageCargo = new PAGE_Cargo(); // admin user cargo
			AdminPageBO adminPageBO = new AdminPageBO();

			ADSFPValidator scsfpValidator = new ADSFPValidator();

			scsfpValidator.validateSearchData(userId);
			if (scsfpValidator.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, scsfpValidator
						.getMessageList());
				return;

			}

			pageCargo.setPage_id(userId);

			admPageCollection = adminPageBO.searchPage(pageCargo);

			// More than 100 records warning message.
			if (adminPageBO.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, adminPageBO.getMessageList());
			}
			
			
			// If data persist is successful then displaylatest data in search
			// page.
			ADSearchPageListView listviewbean = new ADSearchPageListView();
			listviewbean.setLanguage(langInd);
			// call the set DisplayData
			listviewbean.setDisplayData(admPageCollection);
			// uniquely name the listview
			listviewbean.setName(AppConstants.ADSearchPageListView_Listview);
			// this oen for the paging
			listviewbean.setPageSize(20);
			// define the columns to be sorted on takes a boolean array
			boolean sortableCols[] = { false, false, false, false };
			listviewbean.setSortableColumns(sortableCols);
			// specify the type of the columns in listview
			int[] dataType = { IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol,
					IListviewFormatter.stringCol };
			listviewbean.setColDataType(dataType);
			pageCollection.put(AppConstants.ADSearchPageListView_Listview, listviewbean);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"searchPage",fe);
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("searchPage");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"searchPage", e);
			throw fe;
		}
		log(ILog.INFO, "PageElementAdminEJBBean::searchPage::End");
	}
	

	
	
	/**
	 * This method is uesd to store the page details.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void storePageDetails(FwTransaction txnBean) throws javax.ejb.EJBException 
	{
		log(ILog.INFO, "PageElementAdminEJBBean::storePageDetails::Started");
		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			// Collection
			String langInd = FwConstants.ENGLISH; // Language Indicator
			if (session != null) {
				Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
				if (httpSession != null) {
					langInd = (String) httpSession.get(AppConstants.LANGUAGE);
				}
			}
			String action = (String) request.get(AppConstants.ACTION); // action
		
			PAGE_Collection admPageColl = (PAGE_Collection) pageCollection.get(AppConstants.PAGE_Collection); 
			PAGE_Cargo pageCargo = admPageColl.getCargo(0); 
		
			pageCargo.setHelp_page_url_adr(FwConstants.SPACE);
			
			ADUPDValidator adupdValidator = new ADUPDValidator();
			adupdValidator.validateStoreData(pageCargo);
			
			if (adupdValidator.hasMessages()) {
				request.put(FwConstants.MESSAGE_LIST, adupdValidator
						.getMessageList());
				pageCollection.put(AppConstants.ADM_PAGE_CARGO, pageCargo);
				pageCollection.put(AppConstants.ACTION, action);
				return;
			}
			if (action != null && action.equalsIgnoreCase("update")) {
				pageCargo.setRowAction(FwConstants.ROWACTION_UPDATE);
			}else{
				pageCargo.setPage_id(pageCargo.getPage_id().toUpperCase());
				pageCargo.setRowAction(FwConstants.ROWACTION_INSERT);
			}

			AdminPageBO adminPageBO = new AdminPageBO();
			PAGE_Collection adminPageCollection = new PAGE_Collection();
			
			adminPageCollection.add(pageCargo);
			adminPageBO.storePageDetails(adminPageCollection);
			
			// If data persist is successful then displaylatest data in search page.
			ADSearchPageListView listviewbean = new ADSearchPageListView();
			listviewbean.setLanguage(langInd);
			// call the set DisplayData
			listviewbean.setDisplayData(adminPageCollection);
			// uniquely name the listview
			listviewbean.setName(AppConstants.ADSearchPageListView_Listview);
			// this oen for the paging
			listviewbean.setPageSize(20);
			// define the columns to be sorted on takes a boolean array
			boolean sortableCols[] = { false, false, false, false };
			listviewbean.setSortableColumns(sortableCols);
			// specify the type of the columns in listview
			int[] dataType = { IListviewFormatter.stringCol,
					IListviewFormatter.stringCol, IListviewFormatter.stringCol,
					IListviewFormatter.stringCol };
			listviewbean.setColDataType(dataType);
			pageCollection.put(AppConstants.ADSearchPageListView_Listview, listviewbean);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);
			
		}catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"storePageDetails",fe);
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("storePageDetails");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"storePageDetails", e);
			throw fe;
		}
		log(ILog.INFO, "PageElementAdminEJBBean::storePageDetails::End");
		}
	
	
	
	/**
	 * This method retrieves and displays all the Pages that match the given
	 * criteria (pageid).
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadPageDetails(FwTransaction txnBean) throws javax.ejb.EJBException 
	{
		log(ILog.INFO, "PageElementAdminEJBBean::loadPageDetails::Start");
		try {
			Map pageCollection = txnBean.getPageCollection(); // Page
			PAGE_Collection admPageColl = (PAGE_Collection) pageCollection.get(AppConstants.PAGE_Collection); 
			PAGE_Cargo pageCargo = admPageColl.getCargo(0); 
			pageCargo.setHelp_page_url_adr(FwConstants.SPACE);
			pageCollection.put(AppConstants.ADM_PAGE_CARGO, pageCargo);
			pageCollection.put(AppConstants.ACTION, "update");
		}catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"loadPageDetails",fe);
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("loadPageDetails");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"loadPageDetails", e);
			throw fe;
		}
		log(ILog.INFO, "PageElementAdminEJBBean::loadPageDetails::End");
	}	
	
	
	
	/**
	 * This method retrieves and displays all the element details that match the given
	 * criteria (pageid).
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadPageElementList(FwTransaction txnBean) throws javax.ejb.EJBException 
	{
		log(ILog.INFO, "PageElementAdminEJBBean::loadPageElementList::Start");
		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			Map beforeCollection=null; // Page
			if (session!=null) {
				beforeCollection = (Map) session
						.get(FwConstants.BEFORE_COLLECTION);
			}
			
			// Collection
			String langInd = FwConstants.ENGLISH; // Language Indicator
			if (session != null) {
				Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
				if (httpSession != null) {
					langInd = (String) httpSession.get(AppConstants.LANGUAGE);
				}
			}
			
			String prevPage = (String) request.get(FwConstants.PREVIOUS_PAGE_ID);
			
			PAGE_Collection admPageColl = (PAGE_Collection) pageCollection.get(AppConstants.PAGE_Collection); 
			if (prevPage.equals("ADAEP")){
				admPageColl = (PAGE_Collection) beforeCollection.get(AppConstants.PAGE_Collection); 
			}
			
			PAGE_Cargo pageCargo = admPageColl.getCargo(0); 
			pageCargo.setHelp_page_url_adr(FwConstants.SPACE);
			pageCollection.put(AppConstants.PAGE_ID, pageCargo.getPage_id());
			pageCollection.put(AppConstants.PAGE_DESCRIPTION, pageCargo.getPage_dsc());
			pageCollection.put(AppConstants.ADM_PAGE_CARGO, pageCargo);
			
			AdminPageBO admPageBO = new AdminPageBO();
			CUSTOM_PAGE_ELE_DATA_Collection pageEleDataCollection = new CUSTOM_PAGE_ELE_DATA_Collection();
			List pageElementList = new ArrayList();
			
			pageEleDataCollection = admPageBO.loadPageElementList(pageCargo.getPage_id(), pageElementList);
			
			pageCollection.put(AppConstants.PAGE_ELE_LIST_FOR_VAL, pageElementList);
			
			// If data persist is successful then displaylatest data in search
			// page.
			ADPageElementDataListView listviewbean = new ADPageElementDataListView();
			listviewbean.setLanguage(langInd);
			// call the set DisplayData
			listviewbean.setDisplayData(pageEleDataCollection);
			// uniquely name the listview
			listviewbean.setName(AppConstants.ADPageElementDataListView_Listview);
			// this oen for the paging
			listviewbean.setPageSize(100);
			// define the columns to be sorted on takes a boolean array

			pageCollection.put(AppConstants.ADPageElementDataListView_Listview, listviewbean);
			if (session!=null) {
				session.put(FwConstants.BEFORE_COLLECTION, pageCollection);
			}

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"loadPageElementList",fe);
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("loadPageElementList");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"loadPageElementList", e);
			throw fe;
		}
		log(ILog.INFO, "PageElementAdminEJBBean::loadPageElementList::End");
	}
	
	/**
	 * This method retrieves and displays all the details of an element that is added to the page.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void loadPageElementDetails(FwTransaction txnBean) throws javax.ejb.EJBException 
	{
		log(ILog.INFO, "PageElementAdminEJBBean::loadPageElementDetails::Start");
		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			Map beforeCollection = null;
			if (session!=null) {
				beforeCollection = (Map) session
						.get(FwConstants.BEFORE_COLLECTION);
			}
			
			String langInd = FwConstants.ENGLISH; // Language Indicator
			if (session != null) {
				Map httpSession = (Map) session.get(FwConstants.HTTP_SESSION);
				if (httpSession != null) {
					langInd = (String) httpSession.get(AppConstants.LANGUAGE);
				}
			}
			AdminPageBO pageBO = new AdminPageBO();
			List rulesForDropdown = new ArrayList();
			
			if (beforeCollection !=null && beforeCollection.get(AppConstants.APP_RULES)!=null){
				rulesForDropdown = (List)beforeCollection.get(AppConstants.APP_RULES);
			}else{
				rulesForDropdown = pageBO.getAppRuleForDropdown(langInd);
			}
			
			String pageAction = (String)request.get(FwConstants.PAGE_ACTION);
			if(pageAction!=null && (pageAction.equalsIgnoreCase("ADPEDAdd")||
					pageAction.equalsIgnoreCase("ADAEPAddMore"))){
				pageCollection.put(FwConstants.ACTION,AppConstants.NEW);
			}else{
				pageCollection.put(FwConstants.ACTION,AppConstants.EXISTING);
			}
			
			pageCollection.put(AppConstants.APP_RULES, rulesForDropdown);			
			
			PAGE_Collection pageColl = new PAGE_Collection();
			PAGE_Cargo pageCargo = new PAGE_Cargo();			
			pageCargo.setPage_id((String)request.get("pageId"));
			pageCargo.setPage_dsc((String)request.get("pageDesc"));
			pageColl.addCargo(pageCargo);			
			pageCollection.put(AppConstants.PAGE_Collection, pageColl);
			
			CUSTOM_PAGE_ELE_DATA_Collection pageEleCustColl = (CUSTOM_PAGE_ELE_DATA_Collection) pageCollection.get(AppConstants.CUSTOM_PAGE_ELE_DATA_Collection);
			CUSTOM_PAGE_ELE_DATA_Cargo pageEleCustCargo = null;		
			PAGE_ELE_Collection pageEleColl = null;
			
			if (pageEleCustColl!=null && pageEleCustColl.size() >0 ){
				pageEleCustCargo = pageEleCustColl.getCargo();
				
				if (pageEleCustCargo.getRule_dsc()!=null && pageEleCustCargo.getRule_dsc().trim().length() > 0){
					pageEleColl = pageBO.getPageElementDetails(pageEleCustCargo.getPage_id(), pageEleCustCargo.getEle_id());
					
				}
			}	
			pageCollection.put(AppConstants.PAGE_ELE_Collection, pageEleColl);
			if (beforeCollection!=null) {
				pageCollection.put(AppConstants.PAGE_ELE_LIST_FOR_VAL,
						beforeCollection
								.get(AppConstants.PAGE_ELE_LIST_FOR_VAL));
			}
			if (session!=null) {
				session.put(FwConstants.BEFORE_COLLECTION, pageCollection);
			}

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"loadPageElementDetails",fe);
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("loadPageElementDetails");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"loadPageElementDetails", e);
			throw fe;
		}
		log(ILog.INFO, "PageElementAdminEJBBean::loadPageElementDetails::End");
	}
	
	/**
	 * This method stores page element details.
	 * 
	 * @param txnBean
	 * @throws javax.ejb.EJBException
	 */
	public void storePageElementDetails(FwTransaction txnBean) throws javax.ejb.EJBException 
	{
		log(ILog.INFO, "PageElementAdminEJBBean::storePageElementDetails::Start");
		try {
			Map session = txnBean.getSession(); // Session Object
			Map request = txnBean.getRequest(); // Request Object
			Map pageCollection = txnBean.getPageCollection(); // Page
			Map beforeCollection = (Map) session.get(FwConstants.BEFORE_COLLECTION);
			
			List pageElementList = new ArrayList();
			String action = AppConstants.EXISTING;
			
			String afbAppType = null;
			String rmcAppType = null;
			String rmbAppType = null;
			String srtAppType = null;
			String lceAppType = null;
			String lcrAppType = null;
			String genAppType = null;
			String admAppType = null;
			
			String afbAppTypeBefore = null;
			String rmcAppTypeBefore = null;
			String rmbAppTypeBefore = null;
			String srtAppTypeBefore = null;
			String lceAppTypeBefore = null;
			String lcrAppTypeBefore = null;			
			String genAppTypeBefore = null;
			String admAppTypeBefore = null;
			
			String eleId = null;
			String txtId = null;
			String maxLength = null;
			
			afbAppType = (String)request.get("appType_afb");
			rmcAppType = (String)request.get("appType_rmc");
			rmbAppType = (String)request.get("appType_rmb");
			srtAppType = (String)request.get("appType_srt");
			lceAppType = (String)request.get("appType_lce");
			lcrAppType = (String)request.get("appType_lcr");			
			genAppType = (String)request.get("appType_gen");
			admAppType = (String)request.get("appType_adm");	
			
			eleId = (String)request.get("referenceValues_eleId");
			txtId = (String)request.get("referenceValues_txtId");
			maxLength = (String)request.get("referenceValues_maxLen");
			
			PAGE_ELE_Collection pageEleColl = (PAGE_ELE_Collection)pageCollection.get(AppConstants.PAGE_ELE_Collection);
			PAGE_ELE_Collection storePageColl = new PAGE_ELE_Collection();

			
			PAGE_ELE_Collection pageEleBeforeCollection = null;
			PAGE_ELE_Cargo pageEleBeforeCargo = null;
			

			
			PAGE_Collection pageColl = new PAGE_Collection();
			PAGE_Cargo pageCargo = new PAGE_Cargo();			
			pageCargo.setPage_id((String)request.get("pageId"));
			pageCargo.setPage_dsc((String)request.get("pageDesc"));
			pageColl.addCargo(pageCargo);			
			pageCollection.put(AppConstants.PAGE_Collection, pageColl);
			
			if(beforeCollection.get(FwConstants.ACTION)!=null){
				action = (String)beforeCollection.get(FwConstants.ACTION);
			}

			
			pageElementList= (List)beforeCollection.get(AppConstants.PAGE_ELE_LIST_FOR_VAL);
			ADAEPValidator adaepValidator = new ADAEPValidator();
			adaepValidator.validateADAEPData(eleId,txtId,maxLength, pageElementList,action,
					afbAppType,rmbAppType,rmcAppType,srtAppType,lceAppType,lcrAppType,genAppType,admAppType);
			
			if (adaepValidator!=null && adaepValidator.hasMessages()) {								
				request.put(FwConstants.MESSAGE_LIST, adaepValidator.getMessageList());				

				PAGE_ELE_Cargo pageEleCargo = null;
				for(int i=0; i<pageEleColl.size(); i++){
					pageEleCargo = (PAGE_ELE_Cargo)pageEleColl.getCargo(i);
					if(pageEleCargo!=null && pageEleCargo.getApp_typ()!=null && pageEleCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_AFB)){
						if(afbAppType!=null && afbAppType.equalsIgnoreCase(FwConstants.APP_TYPE_AFB)){
							storePageColl.add(pageEleCargo);
						}
					}else if(pageEleCargo!=null && pageEleCargo.getApp_typ()!=null && pageEleCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_RMC)){
						if(rmcAppType!=null && rmcAppType.equalsIgnoreCase(FwConstants.APP_TYPE_RMC)){
							storePageColl.add(pageEleCargo);
						}
					}else if(pageEleCargo!=null && pageEleCargo.getApp_typ()!=null && pageEleCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_RMB)){
						if(rmbAppType!=null && rmbAppType.equalsIgnoreCase(FwConstants.APP_TYPE_RMB)){
							storePageColl.add(pageEleCargo);
						}
					}else if(pageEleCargo!=null && pageEleCargo.getApp_typ()!=null && pageEleCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_SRT)){
						if(srtAppType!=null && srtAppType.equalsIgnoreCase(FwConstants.APP_TYPE_SRT)){
							storePageColl.add(pageEleCargo);
						}
					}else if(pageEleCargo!=null && pageEleCargo.getApp_typ()!=null && pageEleCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_LCE)){
						if(lceAppType!=null && lceAppType.equalsIgnoreCase(FwConstants.APP_TYPE_LCE)){
							storePageColl.add(pageEleCargo);
						}
					}else if(pageEleCargo!=null && pageEleCargo.getApp_typ()!=null && pageEleCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_LCR)){
						if(lcrAppType!=null && lcrAppType.equalsIgnoreCase(FwConstants.APP_TYPE_LCR)){
							storePageColl.add(pageEleCargo);
						}						
					}else if(pageEleCargo!=null && pageEleCargo.getApp_typ()!=null && pageEleCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_GEN)){
						if(genAppType!=null && genAppType.equalsIgnoreCase(FwConstants.APP_TYPE_GEN)){
							storePageColl.add(pageEleCargo);
						}
					}else if(pageEleCargo!=null && pageEleCargo.getApp_typ()!=null && pageEleCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_ADM)){
						if(admAppType!=null && admAppType.equalsIgnoreCase(FwConstants.APP_TYPE_ADM)){
							storePageColl.add(pageEleCargo);
						}
					}
				}				
				
				// if the app type not selected
				if(storePageColl==null || storePageColl.size()==0){
					pageEleCargo = new PAGE_ELE_Cargo();
					pageEleCargo.setPage_id(pageCargo.getPage_id());
					pageEleCargo.setEle_id(eleId);
					pageEleCargo.setTxt_id(txtId);
					pageEleCargo.setMax_lth(maxLength);
					if(storePageColl==null){
						storePageColl = new PAGE_ELE_Collection();
					}
					storePageColl.add(pageEleCargo);
				}
				
				pageCollection.put(AppConstants.PAGE_ELE_Collection, storePageColl);
				
				pageCollection.put(AppConstants.PAGE_ELE_LIST_FOR_VAL, beforeCollection.get(AppConstants.PAGE_ELE_LIST_FOR_VAL));
				pageCollection.put(AppConstants.APP_RULES, beforeCollection.get(AppConstants.APP_RULES));				
				pageCollection.put(AppConstants.CUSTOM_PAGE_ELE_DATA_Collection,
						beforeCollection.get(AppConstants.CUSTOM_PAGE_ELE_DATA_Collection));
				pageCollection.put(FwConstants.ACTION, beforeCollection.get(FwConstants.ACTION));
				return;				
			}			


			if (beforeCollection.get(AppConstants.PAGE_ELE_Collection) != null) {
				pageEleBeforeCollection = (PAGE_ELE_Collection) beforeCollection.get(AppConstants.PAGE_ELE_Collection);
			}
			
			if(pageEleBeforeCollection!=null && pageEleBeforeCollection.size()>0){
				PAGE_ELE_Cargo pageEleCargo = null;

				for(int i=0; i<pageEleBeforeCollection.size(); i++){
					
					
						pageEleBeforeCargo = (PAGE_ELE_Cargo) pageEleBeforeCollection
								.getCargo(i);
						if (pageEleBeforeCargo!=null) {
						pageEleBeforeCargo.setPage_id(pageCargo.getPage_id());
						pageEleBeforeCargo.setEle_id(eleId);
						pageEleBeforeCargo.setTxt_id(txtId);
						pageEleBeforeCargo.setMax_lth(maxLength);
					}
					if(pageEleBeforeCargo!=null && pageEleBeforeCargo.getApp_typ()!=null && pageEleBeforeCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_AFB)){
						afbAppTypeBefore = AppConstants.YES;
						if(afbAppType==null || afbAppType.trim().length()==0){
							pageEleBeforeCargo.setRowAction(FwConstants.ROWACTION_DELETE);	
							storePageColl.add(pageEleBeforeCargo);
						}
					} else if(pageEleBeforeCargo!=null && pageEleBeforeCargo.getApp_typ()!=null && pageEleBeforeCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_RMB)){
						rmbAppTypeBefore = AppConstants.YES;
						if(rmbAppType==null || rmbAppType.trim().length()==0){
							pageEleBeforeCargo.setRowAction(FwConstants.ROWACTION_DELETE);	
							storePageColl.add(pageEleBeforeCargo);
						}
					} else if(pageEleBeforeCargo!=null && pageEleBeforeCargo.getApp_typ()!=null && pageEleBeforeCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_RMC)){
						rmcAppTypeBefore = AppConstants.YES;
						if(rmcAppType==null || rmcAppType.trim().length()==0){
							pageEleBeforeCargo.setRowAction(FwConstants.ROWACTION_DELETE);
							storePageColl.add(pageEleBeforeCargo);
						}
					} else if(pageEleBeforeCargo!=null && pageEleBeforeCargo.getApp_typ()!=null && pageEleBeforeCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_SRT)){
						srtAppTypeBefore = AppConstants.YES;
						if(srtAppType==null || srtAppType.trim().length()==0){
							pageEleBeforeCargo.setRowAction(FwConstants.ROWACTION_DELETE);
							storePageColl.add(pageEleBeforeCargo);
						}
					} else if(pageEleBeforeCargo!=null && pageEleBeforeCargo.getApp_typ()!=null && pageEleBeforeCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_LCE)){
						lceAppTypeBefore = AppConstants.YES;
						if(lceAppType==null || lceAppType.trim().length()==0){
							pageEleBeforeCargo.setRowAction(FwConstants.ROWACTION_DELETE);
							storePageColl.add(pageEleBeforeCargo);
						}
					} else if(pageEleBeforeCargo!=null && pageEleBeforeCargo.getApp_typ()!=null && pageEleBeforeCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_LCR)){
						lcrAppTypeBefore = AppConstants.YES;
						if(lcrAppType==null || lcrAppType.trim().length()==0){
							pageEleBeforeCargo.setRowAction(FwConstants.ROWACTION_DELETE);
							storePageColl.add(pageEleBeforeCargo);
						}						
					}  else if(pageEleBeforeCargo!=null && pageEleBeforeCargo.getApp_typ()!=null && pageEleBeforeCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_GEN)){
						genAppTypeBefore = AppConstants.YES;
						if(genAppType==null || genAppType.trim().length()==0){
							pageEleBeforeCargo.setRowAction(FwConstants.ROWACTION_DELETE);
							storePageColl.add(pageEleBeforeCargo);
						}
					}  else if(pageEleBeforeCargo!=null && pageEleBeforeCargo.getApp_typ()!=null && pageEleBeforeCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_ADM)){
						admAppTypeBefore = AppConstants.YES;
						if(admAppType==null || admAppType.trim().length()==0){
							pageEleBeforeCargo.setRowAction(FwConstants.ROWACTION_DELETE);	
							storePageColl.add(pageEleBeforeCargo);
						}
					} 
					
				}
				for(int j=0; j<pageEleColl.size(); j++){
					pageEleCargo = (PAGE_ELE_Cargo)pageEleColl.getCargo(j);
					pageEleCargo.setPage_id(pageCargo.getPage_id());
					pageEleCargo.setEle_id(eleId);
					pageEleCargo.setTxt_id(txtId);
					pageEleCargo.setMax_lth(maxLength);
					
					// temp remove later -- start
					pageEleCargo.setMa_ind("0");
					pageEleCargo.setTf_ind("0");
					pageEleCargo.setWc_ind("0");
					pageEleCargo.setAg_ind("0");
					pageEleCargo.setMh_ind("0");
					pageEleCargo.setTe_ind("0");
					pageEleCargo.setEa_ind("0");
					pageEleCargo.setSa_ind("0");
					pageEleCargo.setAr_ind("0");
					pageEleCargo.setSc_ind("0");
					pageEleCargo.setFp_ind("0");
					pageEleCargo.setBg_ind("0");
					pageEleCargo.setDc_ind("0");
					pageEleCargo.setAm_ind("0");
					// temp remove later -- end
					
					if (pageEleCargo.getRule_id()==null ||pageEleCargo.getRule_id().equals(FwConstants.SPACE)
							||pageEleCargo.getRule_id().equals(AppConstants.SELECT_DEFAULT_OPTION)){
						pageEleCargo.setRule_id(AppConstants.ZERO);
					}
					
					if(pageEleCargo!=null && pageEleCargo.getApp_typ()!=null && pageEleCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_AFB)){
						if(afbAppType!=null && afbAppType.equalsIgnoreCase(FwConstants.APP_TYPE_AFB)){
							if(afbAppTypeBefore==null){
								pageEleCargo.setRowAction(FwConstants.ROWACTION_INSERT);
							}else{
								pageEleCargo.setRowAction(FwConstants.ROWACTION_UPDATE);
							}
							storePageColl.add(pageEleCargo);
						}
					}else if(pageEleCargo!=null && pageEleCargo.getApp_typ()!=null && pageEleCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_RMB)){
						if(rmbAppType!=null && rmbAppType.equalsIgnoreCase(FwConstants.APP_TYPE_RMB)){
							if(rmbAppTypeBefore==null){
								pageEleCargo.setRowAction(FwConstants.ROWACTION_INSERT);
							}else{
								pageEleCargo.setRowAction(FwConstants.ROWACTION_UPDATE);
							}
							storePageColl.add(pageEleCargo);
						}
					}else if(pageEleCargo!=null && pageEleCargo.getApp_typ()!=null && pageEleCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_RMC)){
						if(rmcAppType!=null && rmcAppType.equalsIgnoreCase(FwConstants.APP_TYPE_RMC)){
							if(rmcAppTypeBefore==null){
								pageEleCargo.setRowAction(FwConstants.ROWACTION_INSERT);
							}else{
								pageEleCargo.setRowAction(FwConstants.ROWACTION_UPDATE);
							}
							storePageColl.add(pageEleCargo);
						}
					}else if(pageEleCargo!=null && pageEleCargo.getApp_typ()!=null && pageEleCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_SRT)){
						if(srtAppType!=null && srtAppType.equalsIgnoreCase(FwConstants.APP_TYPE_SRT)){
							if(srtAppTypeBefore==null){
								pageEleCargo.setRowAction(FwConstants.ROWACTION_INSERT);
							}else{
								pageEleCargo.setRowAction(FwConstants.ROWACTION_UPDATE);
							}
							storePageColl.add(pageEleCargo);
						}
					}else if(pageEleCargo!=null && pageEleCargo.getApp_typ()!=null && pageEleCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_LCE)){
						if(lceAppType!=null && lceAppType.equalsIgnoreCase(FwConstants.APP_TYPE_LCE)){
							if(lceAppTypeBefore==null){
								pageEleCargo.setRowAction(FwConstants.ROWACTION_INSERT);
							}else{
								pageEleCargo.setRowAction(FwConstants.ROWACTION_UPDATE);
							}
							storePageColl.add(pageEleCargo);
						}
					}else if(pageEleCargo!=null && pageEleCargo.getApp_typ()!=null && pageEleCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_LCR)){
						if(lcrAppType!=null && lcrAppType.equalsIgnoreCase(FwConstants.APP_TYPE_LCR)){
							if(lcrAppTypeBefore==null){
								pageEleCargo.setRowAction(FwConstants.ROWACTION_INSERT);
							}else{
								pageEleCargo.setRowAction(FwConstants.ROWACTION_UPDATE);
							}
							storePageColl.add(pageEleCargo);
						}						
					}else if(pageEleCargo!=null && pageEleCargo.getApp_typ()!=null && pageEleCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_GEN)){
						if(genAppType!=null && genAppType.equalsIgnoreCase(FwConstants.APP_TYPE_GEN)){
							if(genAppTypeBefore==null){
								pageEleCargo.setRowAction(FwConstants.ROWACTION_INSERT);
							}else{
								pageEleCargo.setRowAction(FwConstants.ROWACTION_UPDATE);
							}
							storePageColl.add(pageEleCargo);
						}
					}else if(pageEleCargo!=null && pageEleCargo.getApp_typ()!=null && pageEleCargo.getApp_typ().equalsIgnoreCase(FwConstants.APP_TYPE_ADM)){
						if(admAppType!=null && admAppType.equalsIgnoreCase(FwConstants.APP_TYPE_ADM)){
							if(admAppTypeBefore==null){
								pageEleCargo.setRowAction(FwConstants.ROWACTION_INSERT);
							}else{
								pageEleCargo.setRowAction(FwConstants.ROWACTION_UPDATE);
							}
							storePageColl.add(pageEleCargo);
						}
					}					
				}	
			}else{
				PAGE_ELE_Cargo pageEleCargo = null;
				for(int i=0; i<pageEleColl.size(); i++){
					pageEleCargo = (PAGE_ELE_Cargo)pageEleColl.getCargo(i);
					if (pageCargo!=null && pageEleCargo!=null) {
						pageEleCargo.setPage_id(pageCargo.getPage_id());
					}
					if (pageEleCargo != null) {
						pageEleCargo.setEle_id(eleId);
						pageEleCargo.setTxt_id(txtId);
						pageEleCargo.setMax_lth(maxLength);

						// temp remove later -- start
						pageEleCargo.setMa_ind("0");
						pageEleCargo.setTf_ind("0");
						pageEleCargo.setWc_ind("0");
						pageEleCargo.setAg_ind("0");
						pageEleCargo.setMh_ind("0");
						pageEleCargo.setTe_ind("0");
						pageEleCargo.setEa_ind("0");
						pageEleCargo.setSa_ind("0");
						pageEleCargo.setAr_ind("0");
						pageEleCargo.setSc_ind("0");
						pageEleCargo.setFp_ind("0");
						pageEleCargo.setBg_ind("0");
						pageEleCargo.setDc_ind("0");
						pageEleCargo.setAm_ind("0");
						// temp remove later -- end

						if (pageEleCargo.getRule_id() == null
								|| pageEleCargo.getRule_id().equals(
										FwConstants.SPACE)
								|| pageEleCargo.getRule_id().equals(
										AppConstants.SELECT_DEFAULT_OPTION)) {
							pageEleCargo.setRule_id(AppConstants.ZERO);
						}
						pageEleCargo.setRowAction(FwConstants.ROWACTION_INSERT);
						if (pageEleCargo != null
								&& pageEleCargo.getApp_typ() != null
								&& pageEleCargo.getApp_typ().equalsIgnoreCase(
										FwConstants.APP_TYPE_AFB)) {
							if (afbAppType != null
									&& afbAppType
											.equalsIgnoreCase(FwConstants.APP_TYPE_AFB)) {
								storePageColl.add(pageEleCargo);
							}
						} else if (pageEleCargo.getApp_typ() != null
								&& pageEleCargo.getApp_typ().equalsIgnoreCase(
										FwConstants.APP_TYPE_RMC)) {
							if (rmcAppType != null
									&& rmcAppType
											.equalsIgnoreCase(FwConstants.APP_TYPE_RMC)) {
								storePageColl.add(pageEleCargo);
							}
						} else if (pageEleCargo.getApp_typ() != null
								&& pageEleCargo.getApp_typ().equalsIgnoreCase(
										FwConstants.APP_TYPE_RMB)) {
							if (rmbAppType != null
									&& rmbAppType
											.equalsIgnoreCase(FwConstants.APP_TYPE_RMB)) {
								storePageColl.add(pageEleCargo);
							}
						} else if (pageEleCargo.getApp_typ() != null
								&& pageEleCargo.getApp_typ().equalsIgnoreCase(
										FwConstants.APP_TYPE_SRT)) {
							if (srtAppType != null
									&& srtAppType
											.equalsIgnoreCase(FwConstants.APP_TYPE_SRT)) {
								storePageColl.add(pageEleCargo);
							}
						} else if (pageEleCargo.getApp_typ() != null
								&& pageEleCargo.getApp_typ().equalsIgnoreCase(
										FwConstants.APP_TYPE_LCE)) {
							if (lceAppType != null
									&& lceAppType
											.equalsIgnoreCase(FwConstants.APP_TYPE_LCE)) {
								storePageColl.add(pageEleCargo);
							}
						} else if (pageEleCargo.getApp_typ() != null
								&& pageEleCargo.getApp_typ().equalsIgnoreCase(
										FwConstants.APP_TYPE_LCR)) {
							if (lcrAppType != null
									&& lcrAppType
											.equalsIgnoreCase(FwConstants.APP_TYPE_LCR)) {
								storePageColl.add(pageEleCargo);
							}
						} else if (pageEleCargo.getApp_typ() != null
								&& pageEleCargo.getApp_typ().equalsIgnoreCase(
										FwConstants.APP_TYPE_GEN)) {
							if (genAppType != null
									&& genAppType
											.equalsIgnoreCase(FwConstants.APP_TYPE_GEN)) {
								storePageColl.add(pageEleCargo);
							}
						} else if (pageEleCargo.getApp_typ() != null
								&& pageEleCargo.getApp_typ().equalsIgnoreCase(
										FwConstants.APP_TYPE_ADM)) {
							if (admAppType != null
									&& admAppType
											.equalsIgnoreCase(FwConstants.APP_TYPE_ADM)) {
								storePageColl.add(pageEleCargo);
							}
						}
					}
				}
			}
			
			
			
			AdminPageBO pageBO = new AdminPageBO();
			pageBO.storePageElementDetails(storePageColl);
			
			if (!pageElementList.contains(eleId)){
				pageElementList.add(eleId);
			}
			pageCollection.put(AppConstants.PAGE_ELE_LIST_FOR_VAL, pageElementList);
			session.put(FwConstants.BEFORE_COLLECTION, pageCollection);
			

		} catch (FwException fe) {
			ctx.setRollbackOnly();
			FwWrappedException we = new FwWrappedException(this.getClass().getName(),"storePageElementDetails",fe);
			we.setCallingClassID(this.getClass().getName());
			we.setCallingMethodID("storePageElementDetails");
			we.setFwException(fe);
			throw we;
		} catch (Exception e) {
			ctx.setRollbackOnly();
			FwException fe = createFwException(this.getClass().getName(),
					"storePageElementDetails", e);
			throw fe;
		}
		log(ILog.INFO, "PageElementAdminEJBBean::storePageElementDetails::End");
	}
	
}
